package ch.heigvd.nutricare.cucumber.utils;

public class TestFailedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public TestFailedException(String message) {
        super(message);
    }

    public TestFailedException() {
        super("The test failed !");
    }
}
