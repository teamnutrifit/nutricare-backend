package ch.heigvd.nutricare.cucumber.utils;

import ch.heigvd.nutricare.api.AuthenticationApi;
import ch.heigvd.nutricare.controller.api.AuthController;
import lombok.Getter;
import org.glassfish.jersey.client.proxy.WebResourceFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.util.Properties;

@Getter
public class ApiService {

    private Client client = ClientBuilder.newClient();
    private WebTarget webTarget;
    private AuthController authController;

    public ApiService() throws IOException {
        Properties properties = new Properties();
        properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
        webTarget = client.target(properties.getProperty("ch.heigvd.nutricare.server.url"));

        authController = new AuthController();
    }

    public AuthenticationApi getAuthenticationApi() {
        return WebResourceFactory.newResource(AuthenticationApi.class, webTarget);
    }

}
