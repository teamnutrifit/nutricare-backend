package ch.heigvd.nutricare.cucumber.steps;

import ch.heigvd.nutricare.api.AuthenticationApi;
import ch.heigvd.nutricare.controller.api.AuthController;
import ch.heigvd.nutricare.cucumber.utils.ApiService;
import ch.heigvd.nutricare.dto.ApiMessageDTO;
import ch.heigvd.nutricare.dto.LoginRequestDTO;
import ch.heigvd.nutricare.dto.LoginSuccessDTO;
import ch.heigvd.nutricare.dto.RegisterDTO;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Objects;

public class RegistrationSteps {
    private ApiService apiService;
    private AuthController api;

    private LoginRequestDTO loginRequestDTO;
    private RegisterDTO registerDTO;

    private Exception lastApiException;
    private int lastStatusCode;

    private ApiMessageDTO lastApiResponse;

    private LoginSuccessDTO lastLoginSuccess;

    private String lastReceivedApiKey = "";

    public RegistrationSteps() throws IOException {
        this.apiService = new ApiService();
        this.api = apiService.getAuthController();
    }


    @Given("I have an authentication server")
    public void iHaveAnAuthenticationServer() {
        Assert.assertNotNull(api);
    }

    @Given("I have a register payload")
    public void iHaveARegisterPayload() {
        registerDTO = new RegisterDTO();
        registerDTO.setFirstname("Jonas1");
        registerDTO.setLastname("Jonar1");
        registerDTO.setPassword("12341");
        registerDTO.setEmail("jojo@nanas.jonas1");
        registerDTO.setBirthdate(LocalDate.now().minusYears(23));
        registerDTO.setUsername("Jonas1");
    }

    @Given("I have a login payload")
    public void iHaveALoginPayload() {
        loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setPassword("12341");
        loginRequestDTO.setUsernameOrEmail("Jonas1");
    }

    @When("I POST the register payload to the REGISTER endpoint")
    public void iPOSTTheRegisterPayloadToTheAuthRegisterEndpoint() {
        try {
            lastApiResponse = api.register(registerDTO).getBody();
            processApiResponse(lastApiResponse);
        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage());
            processException(e);
        }
    }

    @Then("I receive a {int} status code")
    public void iReceiveAStatusCode(int code) {
        Assert.assertEquals(code, lastStatusCode);
    }

    @And("I receive an ApiMessage payload")
    public void iReceiveAnApiMessagePayload() {
        Assert.assertNotNull(lastApiResponse);
    }

    @When("I send a POST to the LOGIN endpoint")
    public void iSendAPOSTToTheAuthLoginEndpoint() {
        try {
            processLoginResponse(Objects.requireNonNull(api.login(loginRequestDTO).getBody()));
        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage());
            processException(e);
        }
    }

    @And("I receive a LoginSuccess payload that has a JWT key")
    public void iReceiveALoginSuccessPayloadThatHasAJWTKey() {
        Assert.assertNotNull(lastLoginSuccess);
        Assert.assertEquals(lastLoginSuccess.getJwt(), lastReceivedApiKey);
    }

    @And("I receive a payload that has the same usernameOrEmail as the login payload")
    public void iReceiveAPayloadThatHasTheSameUsernameOrEmailAsTheLoginPayload() {

    }

    protected void processException(Exception e) {
        lastReceivedApiKey = null;
        lastLoginSuccess = null;
        lastApiResponse = null;
        lastApiException = e;
        lastStatusCode = -1;
        // TODO
    }

    private void processApiResponse(ApiMessageDTO apiResponse) {
        lastApiResponse = apiResponse;
        lastApiException = null;
        lastStatusCode = lastApiResponse.getCode();
    }

    private void processLoginResponse(LoginSuccessDTO loginSuccessDTO) {
        lastReceivedApiKey = loginSuccessDTO.getJwt();
        lastLoginSuccess = loginSuccessDTO;
    }

}
