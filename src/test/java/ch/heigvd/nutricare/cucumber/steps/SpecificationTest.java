package ch.heigvd.nutricare.cucumber.steps;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features="src/test/java/ch/heigvd/nutricare/cucumber/scenarios",
        plugin = {"pretty", "html:target/cucumber"})
public class SpecificationTest {
}
