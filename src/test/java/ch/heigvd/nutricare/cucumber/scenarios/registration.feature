Feature: Register as a simple User

  Background:
    Given I have an authentication server
    Given I have a register payload
    Given I have a login payload

  Scenario: A User creation
    When I POST the register payload to the REGISTER endpoint
    Then I receive a 201 status code
    And  I receive an ApiMessage payload

  Scenario: Login as the User
    When I send a POST to the LOGIN endpoint
    Then I receive a 201 status code
    And  I receive a LoginSuccess payload that has a JWT key
    And  I receive a payload that has the same usernameOrEmail as the login payload
