package ch.heigvd.nutricare.unit;

import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.sex.ESex;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {
    private LocalDate localDate = LocalDate.now();
    private User user = new User(
            1,
            "Username",
            "Firstname",
            "Lastname",
            localDate,
            "Email",
            "Password",
            ESex.FEMALE);

    @Test
    public void userEntityUsernameShouldBeCorrect() {
        assertEquals("Username", user.getUsername());
        user.setUsername("New Username");
        assertEquals("New Username", user.getUsername());
    }

    @Test
    public void userEntityFirstnameShouldBeCorrect() {
        assertEquals("Firstname", user.getFirstname());
        user.setFirstname("New Firstname");
        assertEquals("New Firstname", user.getFirstname());
    }

    @Test
    public void userEntityLastnameShouldBeCorrect() {
        assertEquals("Lastname", user.getLastname());
        user.setLastname("New Lastname");
        assertEquals("New Lastname", user.getLastname());
    }

    @Test
    public void userEntityEmailSetterShouldBeCorrect() {
        assertEquals("Email", user.getEmail());
        user.setEmail("New Email");
        assertEquals("New Email", user.getEmail());
    }

    @Test
    public void userEntityPasswordSetterShouldBeCorrect() {
        assertEquals("Password", user.getPassword());
        user.setPassword("New Password");
        assertEquals("New Password", user.getPassword());
    }

    @Test
    public void userEntityBirthdateSetterShouldBeCorrect() {
        assertEquals(localDate, user.getBirthdate());
        localDate = localDate.plusDays(1000);
        user.setBirthdate(localDate);
        assertEquals(localDate, user.getBirthdate());
    }
}
