package ch.heigvd.nutricare.unit;

import ch.heigvd.nutricare.model.consommation.nutrition.Food;
import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Category;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Instruction;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Recipe;
import ch.heigvd.nutricare.model.consommation.nutrition.utils.EUnit;
import ch.heigvd.nutricare.model.image.Image;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.sex.ESex;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class RecipeTest {
    private final User user = new User(1, "Sender Username", "Sender Firstname", "Sender Lastname", LocalDate.now(), "Sender Email", "Sender Password", ESex.FEMALE);

    private final byte[] i = new byte[]{1, 2, 3};
    private final Category category = new Category(1, "Neutre", null);

    private final Instruction i1 = new Instruction(1, "Mettre de l'eau dans un plat");
    private final Instruction i2 = new Instruction(2, "Transférer l'eau dans une casserole");
    private final Instruction i3 = new Instruction(3, "Faire bouillir l'eau");
    private final Instruction i4 = new Instruction(4, "Bon app!");

    private final Food food1 = new Food("Pomme", "Miam", 0, 12, 22.2, 3.5, 22.2);
    private final Food food2 = new Food("Poire", "Miam", 10, 2, 12.6, 2.2, 3.5);
    private final Food food3 = new Food("Peche", "Miam", 2, 3, 142, 22.32, 3.5);
    private final Food food4 = new Food("Epinard", "Miam", 3, 2, 1, 22.31, 3.5);

    private final FoodIn f1 = new FoodIn(EUnit.GRAMS, 2, food1);
    private final FoodIn f2 = new FoodIn(EUnit.KILO_GRAMS, 2.3, food2);
    private final FoodIn f3 = new FoodIn(EUnit.LITER, 0.3, food3);
    private final FoodIn f4 = new FoodIn(EUnit.QUANTITY, 42, food4);

    private final Recipe recipe = new Recipe(1, "Name", category, Arrays.asList(i1, i2, i3, i4), user, new LinkedList<>(), Arrays.asList(f1, f2, f3, f4), null);

    @Test
    public void recipeEntityAuthorShouldBeCorrect() {
        assertEquals(user, recipe.getAuthor());
    }

    @Test
    public void recipeEntityNameShouldBeCorrect() {
        assertEquals("Name", recipe.getName());
        recipe.setName("New Name");
        assertEquals("New Name", recipe.getName());
    }

    @Test
    public void recipeCategoryShouldBeCorrect() {
        Category c = new Category(2, "Beurk", null);
        assertEquals(category, recipe.getCategory());
        recipe.setCategory(c);
        assertEquals(c, recipe.getCategory());
    }

    @Test
    public void recipeInstructionsShouldBeCorrect() {
        Instruction ci = new Instruction(5, "Beurk");
        assertEquals(Arrays.asList(i1, i2, i3, i4), recipe.getInstructions());
        assertFalse(recipe.getInstructions().contains(ci));
    }

    @Test
    public void recipeFoodsShouldBeCorrect() {
        Food ci = new Food("Beurk", "Miam", 1, 1, 1, 1, 1);
        assertEquals(Arrays.asList(f1, f2, f3, f4), recipe.getFoodList());
        assertFalse(recipe.getFoodList().contains(ci));
    }
}
