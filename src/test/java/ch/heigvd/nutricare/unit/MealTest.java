package ch.heigvd.nutricare.unit;

import ch.heigvd.nutricare.model.consommation.Recommandation;
import ch.heigvd.nutricare.model.consommation.nutrition.Food;
import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.Meal;
import ch.heigvd.nutricare.model.consommation.nutrition.utils.EUnit;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.sex.ESex;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class MealTest {
    private final Recommandation rec = new Recommandation(1, 0, 0, 0);
    private final User user = new User(1, "Sender Username", "Sender Firstname", "Sender Lastname", LocalDate.now(), "Sender Email", "Sender Password", ESex.MALE);

    private final Food food1 = new Food("Pomme","Miam", 0,  0, 12, 22.2, 3.5);
    private final Food food2 = new Food("Poire", "Miam", 1, 2, 12.6, 2.2, 3.5);
    private final Food food3 = new Food("Peche", "Miam", 20, 3, 142, 22.32, 3.5);
    private final Food food4 = new Food("Epinard", "Miam", 5, 2, 1, 22.31, 3.5);

    private final FoodIn f1 = new FoodIn(EUnit.GRAMS, 2, food1);
    private final FoodIn f2 = new FoodIn(EUnit.KILO_GRAMS, 2.3, food2);
    private final FoodIn f3 = new FoodIn(EUnit.LITER, 0.3, food3);
    private final FoodIn f4 = new FoodIn(EUnit.QUANTITY, 42, food4);

    private LocalDateTime n = LocalDateTime.now();

    private final Meal meal = new Meal(1, "Name", n, user, Arrays.asList(f1, f2, f3, f4));

    @Test
    public void mealEntityUserShouldBeCorrect() {
        assertEquals(user, meal.getUser());
    }

    @Test
    public void mealEntityNameShouldBeCorrect() {
        assertEquals("Name", meal.getName());
        meal.setName("New Name");
        assertEquals("New Name", meal.getName());
    }

    @Test
    public void mealFoodsShouldBeCorrect() {
        Food ci = new Food("Beurk", "Miam", 1, 1, 1, 1, 1);
        assertEquals(Arrays.asList(f1, f2, f3, f4), meal.getFoodList());
        assertFalse(meal.getFoodList().contains(ci));
    }

    @Test
    public void mealConsumptionDateShouldBeCorrect() {
        assertEquals(n, meal.getDateTimeOfConsumption());
        assertNotEquals(LocalDate.now().minusWeeks(12), meal.getDateTimeOfConsumption());
    }
}
