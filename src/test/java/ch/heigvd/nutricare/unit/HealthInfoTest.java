package ch.heigvd.nutricare.unit;

import ch.heigvd.nutricare.model.consommation.Recommandation;
import ch.heigvd.nutricare.model.health.HealthInfo;
import ch.heigvd.nutricare.model.health.Weight;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.sex.ESex;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class HealthInfoTest {
    private final LocalDate localDate = LocalDate.now();
    private final Recommandation rec = new Recommandation(1, 0, 0, 0);
    private final User user = new User(
            1,
            "Username",
            "Firstname",
            "Lastname",
            localDate,
            "Email",
            "Password",
            ESex.FEMALE);
    private final double[] w = {55, 56, 55.5, 55};
    private final LocalDate[] d = {
            localDate.minusMonths(1),
            localDate.minusMonths(2),
            localDate.minusMonths(3),
            localDate.minusMonths(4)};
    private final Weight w1 = new Weight(1, w[0], d[0]);
    private final Weight w2 = new Weight(2, w[1], d[1]);
    private final Weight w3 = new Weight(3, w[2], d[2]);
    private final Weight w4 = new Weight(4, w[3], d[3]);

    private final List<Weight> weights = Arrays.asList(w1, w2, w3, w4);
    private HealthInfo healthInfo = new HealthInfo(
        1,
        "Lose weight",
        "Low",
        176,
        weights,
        user);

    @Test
    public void userShouldBeCorrect() {
        assertEquals(user, healthInfo.getUser());
    }

    @Test
    public void goalShouldBeCorrect() {
        assertEquals("Lose weight", healthInfo.getGoal());
        healthInfo.setGoal("Gain weight");
        assertEquals("Gain weight", healthInfo.getGoal());
    }

    @Test
    public void activityLevelShouldBeCorrect() {
        assertEquals("Low", healthInfo.getActivityLevel());
        healthInfo.setActivityLevel("High");
        assertEquals("High", healthInfo.getActivityLevel());
    }

    @Test
    public void heightShouldBeCorrect() {
        assertEquals(176, healthInfo.getHeight());
        healthInfo.setHeight(176.4);
        assertEquals(176.4, healthInfo.getHeight());
    }

    @Test
    public void healthInfoWeightsShouldBeCorrect() {
        assertEquals(weights, healthInfo.getWeightEntities());
        assertNotEquals(Arrays.asList(w1, w2, w3, w4, w1), healthInfo.getWeightEntities());
    }

    @RepeatedTest(4)
    public void allWeightsValuesShouldBeCorrect(RepetitionInfo repetitionInfo) {
        int pos = repetitionInfo.getCurrentRepetition() - 1;
        assertEquals(w[pos], weights.get(pos).getWeight());
    }

    @RepeatedTest(4)
    public void allWeightDatesShouldBeCorrect(RepetitionInfo repetitionInfo) {
        int pos = repetitionInfo.getCurrentRepetition() - 1;
        assertEquals(d[pos], weights.get(pos).getWeightDate());
    }
}
