package ch.heigvd.nutricare.unit;

import ch.heigvd.nutricare.model.image.Image;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ImageTest {
    private final String i = "image";
    private Image image = new Image(1, "image");

    @Test
    public void idShouldBeCorrect() {
        assertEquals(1, image.getIdImage());
    }


    @Test
    public void imageShouldBeCorrect() {
        assertEquals(i, image.getImage());
        image.setImage("new byte[]{1, 2, 3, 4, 5}");
        assertNotEquals("new byte[]{1, 2, 3}", image.getImage());
    }
}
