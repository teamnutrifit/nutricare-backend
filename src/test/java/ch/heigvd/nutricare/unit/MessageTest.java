package ch.heigvd.nutricare.unit;


import ch.heigvd.nutricare.model.message.Message;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.sex.ESex;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageTest {
    private User sender = new User(1, "Sender Username", "Sender Firstname", "Sender Lastname", LocalDate.now(), "Sender Email", "Sender Password", ESex.FEMALE);
    private User receptor = new User(2, "Receptor Username", "Receptor Firstname", "Receptor Lastname", LocalDate.now(), "Receptor Email", "Receptor Password", ESex.MALE);

    private Message message = new Message(1, "Title", "Text", sender, receptor, false);

    @Test
    public void messageEntityUsersShouldBeCorrect() {
        assertEquals(sender, message.getSender());
        assertEquals(receptor, message.getReceptor());
    }

    @Test
    public void messageEntityTitleShouldBeCorrect() {
        assertEquals("Title", message.getTitle());
        message.setTitle("New Title");
        assertEquals("New Title", message.getTitle());
    }

    @Test
    public void messageEntityTextShouldBeCorrect() {
        assertEquals("Text", message.getText());
        message.setText("New Text");
        assertEquals("New Text", message.getText());
    }
}
