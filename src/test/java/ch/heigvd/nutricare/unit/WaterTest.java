package ch.heigvd.nutricare.unit;

import ch.heigvd.nutricare.model.consommation.Water;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.sex.ESex;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class WaterTest {
    private final User user = new User(1, "Sender Username", "Sender Firstname", "Sender Lastname", LocalDate.now(), "Sender Email", "Sender Password", ESex.FEMALE);

    private final LocalDate n = LocalDate.now();

    private final Water water = new Water(0.3, n, user);

    @Test
    public void waterEntityUserShouldBeCorrect() {
        assertEquals(user, water.getUser());
    }

    @Test
    public void waterQuantityShouldBeCorrect() {
        assertEquals(0.3, water.getQuantity());
        water.setQuantity(1.1);
        assertEquals(1.1, water.getQuantity());
    }

    @Test
    public void waterDateShouldBeCorrect() {
        assertEquals(n, water.getDateOfConsumption());
        assertNotEquals(LocalDate.now().minusMonths(213), water.getDateOfConsumption());
    }

}
