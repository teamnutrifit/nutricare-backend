package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.consommation.Water;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface WaterRepository extends JpaRepository<Water, Long> {
    List<Water> findAllByUser(User user);
    Optional<Water> findByUserAndDateOfConsumption(User user, LocalDate date);
}
