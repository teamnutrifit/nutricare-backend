package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.consommation.Recommandation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecommandationRepository extends JpaRepository<Recommandation, Long> {
}
