package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Category;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Recipe;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<Recipe> findByCategoryAndAuthorOrSubscribersContains(Category category, User author, User subscriber);
    List<Recipe> findByAuthor(User user);
    Recipe findByName(String name);
}
