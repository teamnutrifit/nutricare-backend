package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.health.HealthInfo;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HealthInfoRepository extends JpaRepository<HealthInfo, Long> {
    List<HealthInfo> findAllByUser(User entity);
}
