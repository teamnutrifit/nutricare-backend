package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Instruction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InstructionRepository extends JpaRepository<Instruction, Long> {
}
