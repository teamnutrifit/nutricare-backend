package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoodInRepository extends JpaRepository<FoodIn, Long> {
}
