package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.message.Message;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    Message findByIdMessage(long idMessage);
    List<Message> findByTitleAndTextContaining(String firstname, String lastname);
    List<Message> findByTitleContaining(String title);
    List<Message> findByTextContaining(String text);
    List<Message> findBySender(User sender);
    List<Message> findByReceptor(User receptor);
    List<Message> findBySenderAndReceptor(User sender, User receptor);
    long deleteByIdMessage(long id);
}
