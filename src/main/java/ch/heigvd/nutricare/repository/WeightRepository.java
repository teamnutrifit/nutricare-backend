package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.health.Weight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeightRepository extends JpaRepository<Weight, Long> {

}
