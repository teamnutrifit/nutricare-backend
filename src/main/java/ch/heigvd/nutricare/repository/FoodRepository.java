package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.consommation.nutrition.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FoodRepository extends JpaRepository<Food, Long> {
    Optional<Food> findByName(String name);
    List<Food> findByNameContainsOrDescriptionContains(String keyword1, String keyword2);
}
