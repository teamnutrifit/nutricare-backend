package ch.heigvd.nutricare.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ch.heigvd.nutricare.model.user.auth.TokenBlacklistEntity;

import java.util.Optional;

@Repository
public interface TokenBlacklistRepository extends JpaRepository<TokenBlacklistEntity, Long> {

    public Optional<TokenBlacklistEntity> findByToken(String token);

}
