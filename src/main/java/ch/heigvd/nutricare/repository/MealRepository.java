package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.consommation.nutrition.Meal;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface MealRepository extends JpaRepository<Meal, Long> {
    List<Meal> findAllByUser(User currentUser);
    List<Meal> findAllByDateTimeOfConsumptionAfterAndUser(LocalDateTime date, User user);
    List<Meal> findAllByDateTimeOfConsumptionBetweenAndUser(LocalDateTime before, LocalDateTime after, User user);
}
