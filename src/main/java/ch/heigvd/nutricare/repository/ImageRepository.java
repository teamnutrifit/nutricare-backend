package ch.heigvd.nutricare.repository;

import ch.heigvd.nutricare.model.image.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    @Override
    <S extends Image> S save(S s);
}
