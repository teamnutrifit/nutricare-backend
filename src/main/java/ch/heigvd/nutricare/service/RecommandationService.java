package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.consommation.Recommandation;
import ch.heigvd.nutricare.repository.RecommandationRepository;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import ch.heigvd.nutricare.service.exception.MethodNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecommandationService {
    @Autowired
    private UserService userService;

    @Autowired
    private RecommandationRepository recommandationRepository;

    public Recommandation getRecommandation() {
        return userService.getCurrentUser().getRecommandation();
    }

    public Recommandation updateRecommandation(Recommandation r) {
        if (r.getIdRecommandation() == userService.getCurrentUser().getRecommandation().getIdRecommandation()) {
            try {
                recommandationRepository.save(r);
                return r;
            } catch (Exception e) {
                throw new InternalErrorException();
            }
        } else {
            throw new MethodNotAllowedException("This recommandation does not belong to the current user.");
        }
    }
}
