package ch.heigvd.nutricare.service.exception;

public class EntityDoesNotExistException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public EntityDoesNotExistException(String message) {
        super(message);
    }

    public EntityDoesNotExistException() {
        super("Entity does not exist !");
    }
}
