package ch.heigvd.nutricare.service.exception;

public class EntityAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EntityAlreadyExistException(String message) {
        super(message);
    }

    public EntityAlreadyExistException() {
        super("Entity already exists !");
    }
}
