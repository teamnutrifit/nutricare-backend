package ch.heigvd.nutricare.service.exception;

public class InternalErrorException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InternalErrorException(String message) {
        super(message);
    }

    public InternalErrorException() {
        super("Internal Error !");
    }
}
