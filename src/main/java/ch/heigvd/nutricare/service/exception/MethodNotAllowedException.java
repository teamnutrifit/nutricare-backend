package ch.heigvd.nutricare.service.exception;

public class MethodNotAllowedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MethodNotAllowedException(String message) {
        super(message);
    }

    public MethodNotAllowedException() {
        super("Method is not allowed !");
    }
}
