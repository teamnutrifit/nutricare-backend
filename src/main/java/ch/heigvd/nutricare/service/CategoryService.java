package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Category;
import ch.heigvd.nutricare.repository.CategoryRepository;
import ch.heigvd.nutricare.service.exception.EntityAlreadyExistException;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private UserService userService;

    public Category createOrSaveCategory(String name, Category category) {
        try {
            Category c = categoryRepository.findByName(name);
            if (c != null && c.getIdCategory() != category.getIdCategory()) {
                throw new EntityAlreadyExistException("Two categories cannot have the same name");
            }
            if (!category.getUsers().contains(userService.getCurrentUser())) {
                category.getUsers().add(userService.getCurrentUser());
            }

            categoryRepository.save(category);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }

        return category;
    }

    public List<Category> getCategories() {
        try {
            return categoryRepository.findByUsersContains(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Category getCategory(String name) {
        try {
            return categoryRepository.findByName(name);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
