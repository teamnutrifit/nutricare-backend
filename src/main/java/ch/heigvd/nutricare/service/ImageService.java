package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Recipe;
import ch.heigvd.nutricare.model.image.Image;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.repository.ImageRepository;
import ch.heigvd.nutricare.repository.RecipeRepository;
import ch.heigvd.nutricare.repository.UserRepository;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import ch.heigvd.nutricare.service.exception.MethodNotAllowedException;
import ch.heigvd.nutricare.service.exception.WrongCredentialsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private ImageRepository imageRepository;

    public Recipe deleteImageRecipe(String name) {
        try {
            Recipe recipe = recipeRepository.findByName(name);
            if (recipe.getAuthor().equals(userService.getCurrentUser())) {
                long imageId = recipe.getImage().getIdImage();
                recipe.setImage(null);
                recipeRepository.save(recipe);
                imageRepository.deleteById(imageId);
                return recipe;
            } else {
                throw new MethodNotAllowedException("The recipe doesn't belong to the current user !");
            }
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public User deleteImageUser() {
        try {
            User user = userService.getCurrentUser();
            long imageId = user.getProfilePic().getIdImage();
            user.setProfilePic(null);
            userRepository.save(user);
            imageRepository.deleteById(imageId);
            return user;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Image getCurrentUserImage() {
        try {
            return userService.getCurrentUser().getProfilePic();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Image getUserImage(String username) {
        try {
            return userService.getUserByUsername(username).getProfilePic();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Image createOrUpdateImageRecipe(String name, Image image) {
        try {
            Recipe recipe = recipeRepository.findByName(name);
            if (recipe.getAuthor().equals(userService.getCurrentUser())) {
                long imageId = recipe.getImage().getIdImage();
                recipe.setImage(image);
                recipeRepository.save(recipe);
                imageRepository.deleteById(imageId);
                return image;
            } else {
                throw new MethodNotAllowedException("The recipe doesn't belong to the current user !");
            }
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Image createOrUpdateImageUser(Image image) {
        try {
            imageRepository.save(image);
            userService.getCurrentUser().setProfilePic(image);
            userRepository.save(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
        return image;
    }
}
