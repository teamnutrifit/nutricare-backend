package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.Meal;
import ch.heigvd.nutricare.repository.MealRepository;
import ch.heigvd.nutricare.service.exception.EntityAlreadyExistException;
import ch.heigvd.nutricare.service.exception.EntityDoesNotExistException;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import ch.heigvd.nutricare.service.exception.MethodNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class MealService {

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private UserService userService;

    public Meal createOrSaveMeal(String name, Meal meal) {
        try {
            meal.setName(name);
            meal.setUser(userService.getCurrentUser());
            mealRepository.save(meal);
            return meal;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public List<Meal> getMeals() {
        try {
            return mealRepository.findAllByUser(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public List<Meal> getMealsDate(String date) {
        LocalDate localDate = LocalDate.parse(date);

        List<Meal> meals = mealRepository.findAllByDateTimeOfConsumptionAfterAndUser(localDate.atStartOfDay(),
                userService.getCurrentUser());

        List<Meal> toRemove = new LinkedList<>();
        meals.forEach(m -> {
            if (!m.getDateTimeOfConsumption().toLocalDate().equals(localDate)) {
                toRemove.add(m);
            }
        });

        toRemove.forEach(meals::remove);

        return meals;
    }

    public Meal updateMeal(Integer id, Meal meal) {
        try {
            Optional<Meal> m = mealRepository.findById(Long.valueOf(id));
            if (m.isEmpty()) {
                throw new EntityDoesNotExistException();
            }
            if (m.get().getUser().getIdUser() != userService.getCurrentUser().getIdUser()) {
                throw new MethodNotAllowedException();
            }
            
            Meal mealFromDB = m.get();

            mealFromDB.setName(meal.getName());
            mealFromDB.setDateTimeOfConsumption(meal.getDateTimeOfConsumption());
            mealRepository.save(mealFromDB);
            
            return mealFromDB;

        } catch (EntityDoesNotExistException | MethodNotAllowedException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Meal addFood(Integer id, FoodIn food) {
        Optional<Meal> meal = mealRepository.findById(id.longValue());
        AtomicBoolean hasFood = new AtomicBoolean(false);
        // if(meal.isEmpty()) {
        // throw new EntityDoesNotExistException();
        // }
        if (meal.get().getUser().getIdUser() != userService.getCurrentUser().getIdUser()) {
            throw new MethodNotAllowedException("The current user does not own this Meal.");
        }

        List<FoodIn> foodInToRemove = new ArrayList<>();

        meal.get().getFoodList().forEach(foodIn -> {
            if (foodIn.getFood().equals(food.getFood())) {
                hasFood.set(true);
                if (food.getQuantity() == 0) {
                    foodInToRemove.add(foodIn);
                } else {
                    foodIn.setQuantity(food.getQuantity());
                    foodIn.setUnit(food.getUnit());
                }
            }
        });

        foodInToRemove.forEach(f -> meal.get().getFoodList().remove(f));

        if (!hasFood.get()) {
            meal.get().getFoodList().add(food);
        }
        try {
            mealRepository.save(meal.get());
        } catch (Exception e) {
            throw new InternalErrorException();
        }
        return meal.get();
    }

    public void deleteMeal(Integer id) {
        Optional<Meal> meal = mealRepository.findById(id.longValue());
        if (meal.isEmpty()) {
            throw new EntityDoesNotExistException();
        } else if (meal.get().getUser().getIdUser() != userService.getCurrentUser().getIdUser()) {
            throw new MethodNotAllowedException("The user does not own this Meal");
        } else {
            mealRepository.delete(meal.get());
        }
    }
}
