package ch.heigvd.nutricare.service;

import java.util.ArrayList;
import java.util.List;

import ch.heigvd.nutricare.model.user.role.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.heigvd.nutricare.model.user.role.ERole;
import ch.heigvd.nutricare.repository.RoleRepository;
import ch.heigvd.nutricare.service.exception.EntityDoesNotExistException;

@Service
public class RoleService {
    @Autowired
    private RoleRepository repository;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Retrieves a valid Sex from the DB. THis is the only way to get a role to use
     * with a user. To be coherent with the DB.
     *
     * @param roleName the name of the role to retrieve
     * @return the role from DB
     * @throws EntityDoesNotExistException if the role is not in the DB. (Which is bad).
     */
    public Role getRole(ERole roleName) throws EntityDoesNotExistException {
        return repository.findByName(roleName).orElseThrow(
                () -> new EntityDoesNotExistException("Sex with name " + roleName + " doesn't exist in DB."));
    }

    /**
     * This method is called to Generate the ROles to the DB. This is used to
     * persist the ESex to the DB.
     */
    public void generateRolesToDatabase() {
        try {
            List<Role> roles = new ArrayList<>();
            for (ERole eRole : ERole.values()) {
                Role role = Role.builder().name(eRole).build();
                roles.add(role);
            }

            for (Role role : roles) {
                if (!repository.existsByName(role.getName())) {
                    repository.save(role);
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
