package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.dto.FoodDTO;
import ch.heigvd.nutricare.model.consommation.nutrition.Food;
import ch.heigvd.nutricare.repository.FoodRepository;
import ch.heigvd.nutricare.repository.UserRepository;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import ch.heigvd.nutricare.service.exception.MethodNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FoodService {

    @Autowired
    private FoodRepository foodRepository;

    public Food createOrSaveFood(Food food) {
        Optional<Food> f = foodRepository.findByName(food.getName());
        if (f.isPresent() && food.getIdFood() != f.get().getIdFood()) {
            throw new MethodNotAllowedException("The foods name must be unique.");
        }
        try {
            foodRepository.save(food);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
        return food;
    }

    public Iterable<Food> getFood() {
        try {
            return foodRepository.findAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Food getFoodByName(String food) {
        try {
            return foodRepository.findByName(food).get();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Iterable<Food> searchFood(String keyword) {
        try {
            return foodRepository.findByNameContainsOrDescriptionContains(keyword, keyword);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
