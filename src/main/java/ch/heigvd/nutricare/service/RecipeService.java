package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Category;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Instruction;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Recipe;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.repository.*;
import ch.heigvd.nutricare.service.exception.EntityAlreadyExistException;
import ch.heigvd.nutricare.service.exception.EntityDoesNotExistException;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import ch.heigvd.nutricare.service.exception.MethodNotAllowedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class RecipeService {
    @Autowired
    private UserService userService;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private InstructionRepository instructionRepository;

    @Autowired
    private FoodInRepository foodInRepository;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    public List<Recipe> getRecipes(String categoryName) {
        try {
            Category category = categoryRepository.findByName(categoryName);
            return recipeRepository.findByCategoryAndAuthorOrSubscribersContains(
                    category,
                    userService.getCurrentUser(),
                    userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Recipe createOrSaveRecipe(String name, Recipe recipe) {
        Category c = categoryRepository.findByName(recipe.getCategory().getName());
        logger.info("Create {}", recipe);
        Recipe r = recipeRepository.findByName(name);
        if (c == null) {
            throw new MethodNotAllowedException("The category does not exist.");
        }
        if (r != null && r.getIdRecipe() != recipe.getIdRecipe()) {
            throw new EntityAlreadyExistException("Two recipes cannot have the same name.");
        }
        if (recipe.getAuthor() != null && !recipe.getAuthor().getUsername().equals(userService.getCurrentUser().getUsername())) {
            throw new MethodNotAllowedException("Only the author can modify the recipe.");
        }
        try {
            recipe.setName(name);
            recipe.setCategory(c);
            recipe.setAuthor(userService.getCurrentUser());
            recipe.setSubscribers(new LinkedList<>());
            recipeRepository.save(recipe);
            logger.info("Created {}", recipe);
            return recipe;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Recipe getRecipe(String name) {
        try {
            return recipeRepository.findByName(name);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public List<Recipe> getAll() {
        try {
            return recipeRepository.findByAuthor(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Recipe addInstruction(String name, Instruction instruction) {
        Recipe r = recipeRepository.findByName(name);
        if(r == null) {
            throw new EntityDoesNotExistException("The recipe does not exist.");
        } else if(r.getAuthor().getIdUser() != userService.getCurrentUser().getIdUser()) {
            throw new MethodNotAllowedException("The recipe does not belong to the current user.");
        }

        try {
            instructionRepository.save(instruction);
            r.getInstructions().add(instruction);
            recipeRepository.save(r);
            return r;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Recipe addFood(String name, FoodIn food) {
        Recipe r = recipeRepository.findByName(name);
        if(r == null) {
            throw new EntityDoesNotExistException("The recipe does not exist.");
        } else if(r.getAuthor().getIdUser() != userService.getCurrentUser().getIdUser()) {
            throw new MethodNotAllowedException("The recipe does not belong to the current user.");
        }

        try {
            foodInRepository.save(food);
            r.getFoodList().add(food);
            recipeRepository.save(r);
            return r;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Recipe subscribeTo(String name) {
        Recipe r = getRecipe(name);
        if (!r.getSubscribers().contains(userService.getCurrentUser())) {
            r.getSubscribers().add(userService.getCurrentUser());
        }
        try {
            recipeRepository.save(r);
            return r;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
