package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.consommation.Water;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.repository.WaterRepository;
import ch.heigvd.nutricare.service.exception.EntityDoesNotExistException;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import ch.heigvd.nutricare.service.exception.MethodNotAllowedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class WaterService {
    @Autowired
    private WaterRepository waterRepository;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Save the given water entity in the water repository
     * @param water the water entity to save in repository
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void save(Water water) {
        Optional<Water> originalWater = waterRepository.findByUserAndDateOfConsumption(
                userService.getCurrentUser(), water.getDateOfConsumption());

        if(originalWater.isPresent()) {
            water.setUser(originalWater.get().getUser());
            water.setIdWater(originalWater.get().getIdWater());

            try {
                waterRepository.save(water);
                logger.info("Created or saved Water");
            } catch (Exception e) {
                throw new InternalErrorException(e.getMessage());
            }
        } else {
            throw new EntityDoesNotExistException("The user has no water consumption for the given day...");
        }
    }

    /**
     * Get the list of water consumption of the current user for the past dayNbr days
     * @param dayNbr the number of days
     * @return a list of water entities
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public List<Water> getFromDays(Integer dayNbr) {
        List<Water> waterEntities = waterRepository.findAllByUser(userService.getCurrentUser());
        waterEntities.removeIf(water ->
            water.getDateOfConsumption().isAfter(LocalDate.now().minusDays(dayNbr))
        );

        return waterEntities;
    }
    /**
     * Get the list of water consumption of the current user
     * @return a list of water entities
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public List<Water> getWater() {
        try {
            return waterRepository.findAllByUser(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public Water getFromDay(LocalDate date) {
        try {
            Optional<Water> water = waterRepository.findByUserAndDateOfConsumption(userService.getCurrentUser(), date);

            if (water.isEmpty()) {
                water = Optional.of(Water.builder()
                        .dateOfConsumption(date)
                        .quantity(0)
                        .user(userService.getCurrentUser())
                        .build());
                waterRepository.save(water.get());
            }

            return water.get();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
