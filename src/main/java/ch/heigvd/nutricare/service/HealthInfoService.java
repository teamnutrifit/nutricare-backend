package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.health.HealthInfo;
import ch.heigvd.nutricare.model.health.Weight;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.repository.HealthInfoRepository;
import ch.heigvd.nutricare.repository.UserRepository;
import ch.heigvd.nutricare.service.exception.EntityDoesNotExistException;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class HealthInfoService {

    @Autowired
    private HealthInfoRepository healthInfoRepository;

    @Autowired
    private UserService userService;

    /**
     * Get health information for the given user
     * @return healthi info entity for the given user
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public HealthInfo getHealthInfo() {
        User user;
        List<HealthInfo> healthInfoEntities;
        try {
            user = userService.getCurrentUser();
            healthInfoEntities = healthInfoRepository.findAllByUser(user);
            return healthInfoEntities.get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new EntityDoesNotExistException("The given User has no health info in the database.");
        } catch (Exception e) {
            throw new InternalErrorException("An unknown error occurred : " + e.getMessage());
        }
    }

    /**
     * Update or create the given health info
     * @param healthInfo the object to save and/or update in the data base
     * @return the saved/updated object
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public HealthInfo updateOrCreateHealthInfo(HealthInfo healthInfo) {

        try {
            Optional<HealthInfo> h = healthInfoRepository.findById(healthInfo.getIdHealthInfo());
            h.ifPresent(info -> healthInfo.setWeightEntities(info.getWeightEntities()));
            healthInfo.setUser(userService.getCurrentUser());
            return healthInfoRepository.save(healthInfo);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Remove the health info of current user from the data base
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void delete() {
        try {
            long id = healthInfoRepository.findAllByUser(userService.getCurrentUser()).get(0).getIdHealthInfo();
            healthInfoRepository.deleteById(id);
        } catch (Exception e) {
            throw new EntityDoesNotExistException("The given Health Info id is invalid : " + e.getMessage());
        }
    }

    /**
     * Get the weight evolution of the current user
     * @return a list of weight entities
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public List<Weight> getWeights() {
        try {
            return getHealthInfo().getWeightEntities();
        } catch (Exception e) {
            throw new EntityDoesNotExistException("The given User id is invalid : " + e.getMessage());
        }
    }

    /**
     * Get the weight evolution of the given user going back to the given date
     * @param oldestDate the given oldest date
     * @return a list of weight entities
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public List<Weight> getWeightsByDate(LocalDate oldestDate) {
        try {
            return getHealthInfo().getWeightEntities().stream().filter(weightEntityDTO ->
                    weightEntityDTO.getWeightDate().isAfter(oldestDate)
            ).collect(java.util.stream.Collectors.toList());
        } catch (Exception e) {
            throw new EntityDoesNotExistException("The given User id is invalid : " + e.getMessage());
        }
    }

    /**
     * Get the last weight entry for the given user
     * @return th elaste weight entry of the user
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public Weight getLastWeight() {
        try {
            HealthInfo healthInfo = getHealthInfo();
            LocalDate mostRecent = LocalDate.ofYearDay(1, 1);
            Weight weight = null;

            for (Weight w : healthInfo.getWeightEntities()) {
                if (w.getWeightDate().isAfter(mostRecent)) {
                    mostRecent = w.getWeightDate();
                    weight = w;
                }
            }
            System.out.println(weight);

            return weight;
        } catch (Exception e) {
            throw new EntityDoesNotExistException("The given User id is invalid : " + e.getMessage());
        }
    }

    /**
     * Update or create the given weight entity
     * @param weight the new weight value
     * @return the new weight entity
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public Weight updateOrCreateWeight(Weight weight) {
        HealthInfo healthInfo;
        try {
            healthInfo = getHealthInfo();
        } catch (Exception e) {
            throw new EntityDoesNotExistException("The given User id is invalid : " + e.getMessage());
        }
        healthInfo.getWeightEntities().add(weight);

        try {
            healthInfoRepository.save(healthInfo);
            return weight;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
