package ch.heigvd.nutricare.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import ch.heigvd.nutricare.model.consommation.Recommandation;
import ch.heigvd.nutricare.model.health.HealthInfo;
import ch.heigvd.nutricare.model.image.Image;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.role.Role;
import ch.heigvd.nutricare.model.user.sex.ESex;
import ch.heigvd.nutricare.repository.HealthInfoRepository;
import ch.heigvd.nutricare.repository.ImageRepository;
import ch.heigvd.nutricare.repository.RecommandationRepository;
import ch.heigvd.nutricare.service.exception.*;
import ch.heigvd.nutricare.utils.RecommandationFormula;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import ch.heigvd.nutricare.config.security.jwt.JwtUtils;
import ch.heigvd.nutricare.config.security.model.UserDetailsImpl;
import ch.heigvd.nutricare.model.user.auth.TokenBlacklistEntity;
import ch.heigvd.nutricare.model.user.role.ERole;
import ch.heigvd.nutricare.repository.UserRepository;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;

/**
 * Here we implement UserDetailsService for Spring Security purposes.
 */
@Slf4j
@Service
public class UserService implements UserDetailsService {
    /**
     * Private static inner class to inject values from application.properties It is
     * used to create the main admin user.
     */
    @Component
    private static class AdminUser {
        @Value("${nutricare.admin.username}")
        private String username;

        @Value("${nutricare.admin.email}")
        private String email;

        @Value("${nutricare.admin.password}")
        private String password;

        @Value("${nutricare.admin.firstname}")
        private String firstname;

        @Value("${nutricare.admin.lastname}")
        private String lastname;
    }

    @Autowired
    private AdminUser adminUser;

    /**
     * Used for Spring Security
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * Used for Spring Security
     */
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HealthInfoRepository healthInfoRepository;

    @Autowired
    private RecommandationRepository recommandationRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private TokenBlacklistService tokenBlacklistService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    /** Used by Spring Security */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        try {
            return UserDetailsImpl.build(user);
        } catch (Exception e) {
            logger.error("Error with loadByUsername : {}", e.getMessage());
            return null;
        }
    }

    private String encryptPassword(String password) {
        return passwordEncoder.encode(password);
    }

    /**
     * Adds a role (that is valid with the DB) to a User. This is the ONLY
     * ALLOWED WAY to add a role to an user. This does not save the user to DB !
     *
     * @param user
     * @param roleName
     * @return a new user instance with the role added
     */
    public User addRole(User user, ERole roleName) {
        log.trace("Adding role {} to user {}", roleName.toString(), user.getUsername());

        Role role = roleService.getRole(roleName);
        return user.toBuilder().role(role).build();
    }

    /**
     * Removes a role (that is valid with the DB) from a User. This is the
     * ONLY ALLOWED WAY to remove a role from an user. This does not save the user
     * to DB !
     *
     * @param user
     * @param roleName
     * @return a new user instance with the role removed
     */
    public User removeRole(User user, ERole roleName) {
        log.trace("Removing role {} to user {}", roleName.toString(), user.getUsername());

        Role role = roleService.getRole(roleName);
        Set<Role> newRoles = user.getRoles().stream().filter(r -> !r.getName().equals(role.getName()))
                .collect(Collectors.toSet());
        return user.toBuilder().roles(newRoles).build();
    }

    /**
     * Register a User if the parameters are correct.
     *
     * @param user the incomplete user without the role and with non encrypted
     *                   password.
     * @throws EntityAlreadyExistException if the username or email is taken
     * @throws InternalErrorException      If something bad happens.
     */
    public void register(User user) throws EntityAlreadyExistException, InternalErrorException {
        if (userRepository.existsByUsername(user.getUsername())) {
            log.trace("Register username already exists {}", user.getUsername());
            throw new EntityAlreadyExistException("Username is already taken !");
        } else if (userRepository.existsByEmail(user.getEmail())) {
            log.trace("Register email already exists {}", user.getEmail());
            throw new EntityAlreadyExistException("Email is already taken !");
        } else if (userRepository.existsById(user.getIdUser())) {
            log.trace("Register id already exists {}", user.getIdUser());
            throw new EntityAlreadyExistException("Id is already taken !");
        } else {
            // Here we have a unique email and username.
            // Check for password requirements here if needed

            try {
                // Encrypt Password
                user = user.withPassword(encryptPassword(user.getPassword()));
                // Add basic and valid USER Sex
                user = addRole(user, ERole.ROLE_USER);

                // Create Image
                try {
                    Image image = new Image();
                    image.setImage("EMPTY");
                    imageRepository.save(image);
                    user.setProfilePic(image);
                } catch (Exception e) {
                    logger.warn("No image");
                }

                // Create Health-Info
                HealthInfo healthInfo = new HealthInfo();
                healthInfo.setUser(user);
                healthInfo.setActivityLevel("N/A");
                healthInfo.setGoal("None");
                healthInfoRepository.save(healthInfo);

                // Create Recommandation
                Recommandation recommandation = new Recommandation();

                int age = LocalDate.now().getYear() -
                        user.getBirthdate().getYear() -
                        (user.getBirthdate().getDayOfYear() > LocalDate.now().getDayOfYear() ? 0 : 1);
                recommandation.setCalories(
                        RecommandationFormula.getCaloriesRecommandation(user.getSex(), 1, 1, age)
                );
                recommandation.setImc(RecommandationFormula.getImcRecommandation(1, 1));
                recommandation.setWater(RecommandationFormula.getWaterRecommandation());

                recommandationRepository.save(recommandation);

                user.setRecommandation(recommandation);

                // add admin as friend
                user.setFriends(new LinkedList<>());
                user.setInvitingFriends(new LinkedList<>());
                user.setInvitedFriends(new LinkedList<>());

                user.getFriends().add(getUserByUsername(adminUser.username));
                getUserByUsername(adminUser.username).getFriends().add(user);

                // Save the user to DB
                userRepository.save(user);
                log.info("Successful register for {}", user);

            } catch (Exception e) {
                if (userRepository.existsByUsername(user.getUsername())) {
                    log.info("Successful register for {}", user.getUsername());
                } else {

                    e.printStackTrace();
                    log.error("{}", e.getMessage());
                    throw new InternalErrorException(e.getMessage());
                }
            }
        }
    }

    /**
     * Generates a JWT to respond to a login request.
     *
     * @param usernameOrEmail Existing username or Email string.
     * @param password        The password from the user (not encrypted).
     * @return JWT that is used to authenticate the user.
     * @throws WrongCredentialsException If the credentials are wrong or the user
     *                                   doesn't exist.
     * @throws InternalErrorException    If something bad happens.
     */
    public String login(String usernameOrEmail, String password)
            throws WrongCredentialsException, InternalErrorException {

        if (usernameOrEmail == null || usernameOrEmail.isEmpty()) {
            throw new IncompleteBodyException();
        }

        try {
            User user = null;

            try {
                logger.info("Login find by email {}", usernameOrEmail);
                user = userRepository.findByEmail(usernameOrEmail).orElseThrow();
            } catch (NoSuchElementException e) {
                // It's not an email it must be the username
                logger.info("Login find by email {} no such element, trying to find by username.", usernameOrEmail);
                user = userRepository.findByUsername(usernameOrEmail).orElseThrow();
            }

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(user.getUsername(),
                    password);
            Authentication authentication = authenticationManager.authenticate(authToken);

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtUtils.generateJwtToken(authentication);

            logger.info("Successful login by {}", usernameOrEmail);

            return jwt;

        } catch (NoSuchElementException e) {
            log.trace("Login find by username {} no such element. Throwing NoSuchElementException.",
                    usernameOrEmail);
            logger.info("Login attempt by {} - username ou email does not exist.", usernameOrEmail);
            throw new WrongCredentialsException();
        } catch (BadCredentialsException e) {
            log.trace("Login bad password provided for existing user {}", usernameOrEmail);
            logger.info("Login attempt by {} with wrong credentials", usernameOrEmail);
            throw new WrongCredentialsException();
        } catch (Exception e) {
            logger.error("Internal Error, original exception message : {}", e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Logs out a user by placing the JWT to the TokenBlacklist.
     *
     * //@param jwtHeader the header containing the Bearer JWT
     * @throws InternalErrorException If something bad happens.
     */
    public void logout() throws InternalErrorException {
        try {

            String jwt = SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
            log.info("JWT to logout : {}", jwt);
            TokenBlacklistEntity tokenBlacklist = TokenBlacklistEntity.builder()
                    .token(jwt)
                    .expiration(jwtUtils.getExpirationTime(jwt))
                    .build();

            tokenBlacklistService.save(tokenBlacklist);
            log.info("Logout successful for user");
        } catch (ExpiredJwtException e) {
            log.warn("Logout attempt with expired token");
            throw new EntityAlreadyExistException("You are already logged out. The Token is expired.");
        } catch (Exception e) {
            log.error("Internal Error, original exception message : {}", e.getMessage());
            throw new InternalErrorException();
        }
    }

    /**
     * Returns the current user of the request from the DB.
     *
     * @return the current user of the request.
     * @throws EntityDoesNotExistException if something bad happens.
     */
    public User getCurrentUser() throws EntityDoesNotExistException {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> optUser = userRepository.findByUsername(username);
        return optUser.orElseThrow(()-> new EntityDoesNotExistException("Current user cannot be retrieved from JWT"));
    }

    /**
     * Creates the main admin user with parameters from application.properties
     */
    public void createMainAdminUser() throws InternalErrorException {
        try {
            if (userRepository.existsByUsername(adminUser.username)) {
                log.info("Existing main admin user, with username: {} password: {}", adminUser.username,
                        adminUser.password);

            } else {
                log.info("Creating main admin user, with username: {} password: {}", adminUser.username,
                        adminUser.password);

                User user = User.builder()
                        .username(adminUser.username)
                        .email(adminUser.email)
                        .password(encryptPassword(adminUser.password))
                        .firstname(adminUser.firstname)
                        .lastname(adminUser.lastname)
                        .birthdate(LocalDate.now())
                        .sex(ESex.FEMALE)
                        .friends(new LinkedList<>())
                        .invitedFriends(new LinkedList<>())
                        .invitingFriends(new LinkedList<>())
                        .build();

                // Create Image
                Image image = new Image();
                image.setImage("IMAGE");
                user.setProfilePic(image);
                imageRepository.save(image);

                // Create Health-Info
                HealthInfo healthInfo = new HealthInfo();
                healthInfo.setUser(user);
                healthInfo.setActivityLevel("N/A");
                healthInfo.setGoal("None");
                healthInfoRepository.save(healthInfo);

                // Create Recommandation
                Recommandation recommandation = new Recommandation();
                int age = 25;/*LocalDate.now().getYear() -
                        user.getBirthdate().getYear() -
                        (user.getBirthdate().getDayOfYear() > LocalDate.now().getDayOfYear() ? 0 : 1);*/
                recommandation.setCalories(
                        RecommandationFormula.getCaloriesRecommandation(user.getSex(), 1, 1, age)
                );
                recommandation.setImc(RecommandationFormula.getImcRecommandation(1, 1));
                recommandation.setWater(RecommandationFormula.getWaterRecommandation());
                recommandationRepository.save(recommandation);

                user = addRole(user, ERole.ROLE_USER);
                user = addRole(user, ERole.ROLE_ADMIN);
                userRepository.save(user);

                try {
                    HealthInfo info = new HealthInfo();
                    info.setUser(user);
                    healthInfoRepository.save(info);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }

        }
        catch (Exception e) {
            log.error("Internal Error, original exception message : {}", e.getMessage());
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Update the given user to its current value
     * @param user the new value of the user
     * @return the new version of the user
     */
    public User updateUser(User user) {
        if(user.getIdUser() != getCurrentUser().getIdUser()) {
            throw new MethodNotAllowedException();
        }

        if(!user.getPassword().equals("")) {
            logger.debug("Password is {}", user.getPassword());
            user.setPassword(encryptPassword(user.getPassword()));
        } else {
            logger.debug("No password");
            user.setPassword(getCurrentUser().getPassword());
        }
        user.setProfilePic(getCurrentUser().getProfilePic());
        user.setFriends(getCurrentUser().getFriends());
        user.setInvitedFriends(getCurrentUser().getInvitedFriends());
        user.setInvitingFriends(getCurrentUser().getInvitingFriends());
        user.setRoles(getCurrentUser().getRoles());
        user.setRecommandation(getCurrentUser().getRecommandation());

        user = userRepository.save(user);
        return user;
    }

    /**
     * Find a user in repository from the given id
     * @param username the username of the user to return
     * @return the user with the given id
     */
    public User getUserByUsername(String username) {
        Optional<User> o = userRepository.findByUsername(username);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new UnknownEntityException("This user id doesn't exist !");
        }
    }

    /**
     * Find all users in the repository
     * @return a list of all users in data base
     */
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Add a friendship between the two users with the given ids
     * @param idUser the id of the first member of the new friendship
     * @param idFriend the id of the second member of the new friendship
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */

    /**
     * Remove the user with the given id from the data base
     * @param username the username of the user to remove from data base
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void deleteUserByUsername(String username) {
        try {
            long id = userRepository.findByUsername(username).get().getIdUser();
            userRepository.deleteById(id);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Remove all users in the data base
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void deleteUsers() {
        try {
            userRepository.deleteAll();
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Get the list of friends for the user with the given id
     * @return a list of users - the friends of the user with the given id
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public List<User> getFriends() {
        try {
            return new LinkedList<>(getCurrentUser().getFriends());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     *
     * @param username
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void acceptFriend(String username) {
        User currentUser = getCurrentUser();
        try {
            User friend = userRepository.findByUsername(username).get();
            if (currentUser.getInvitingFriends().contains(friend) &&
                    friend.getInvitedFriends().contains(currentUser)) {

                currentUser.getFriends().add(friend);
                currentUser.getInvitingFriends().remove(friend);

                friend.getFriends().add(currentUser);
                friend.getInvitedFriends().remove(currentUser);

                userRepository.save(friend);
                userRepository.save(currentUser);
            } else {
                throw new EntityDoesNotExistException("This friend request has either not been received or sent or both.");
            }
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     *
     * @param username
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void inviteFriend(String username) {
        User currentUser = getCurrentUser();
        logger.info("{} asked {} as a friend", currentUser.getUsername(), username);
        if (username.equals(adminUser.username)) {
            throw new MethodNotAllowedException("You should already be friends with our administrator.");
        }
        try {
            User friend = userRepository.findByUsername(username).get();
            if (currentUser.getInvitingFriends().contains(friend) ||
                    friend.getInvitedFriends().contains(currentUser)) {
                throw new EntityAlreadyExistException("This friend request has already been sent.");
            }
            else if (!currentUser.getInvitingFriends().contains(friend) &&
                    !friend.getInvitedFriends().contains(currentUser)) {
                currentUser.getInvitedFriends().add(friend);
                friend.getInvitingFriends().add(currentUser);

                userRepository.save(friend);
                userRepository.save(currentUser);
            } else {
                throw new InternalErrorException("This friend request has a weird status.");
            }
        } catch (EntityAlreadyExistException | InternalErrorException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     *
     * @param username
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void removeFriend(String username) {
        User currentUser = getCurrentUser();
        try {
            User friend = userRepository.findByUsername(username).get();
            if (currentUser.getFriends().contains(friend) &&
                    friend.getFriends().contains(currentUser)) {
                currentUser.getFriends().remove(friend);
                friend.getFriends().remove(currentUser);

                userRepository.save(friend);
                userRepository.save(currentUser);
            } else {
                throw new EntityDoesNotExistException("This friend request has either not been received or sent or both.");
            }
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     *
     * @param username
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void cancelFriendRequest(String username) {
        User currentUser = getCurrentUser();
        try {
            User friend = userRepository.findByUsername(username).get();
            if (currentUser.getInvitedFriends().contains(friend) &&
                    friend.getInvitingFriends().contains(currentUser)) {
                currentUser.getInvitedFriends().remove(friend);
                friend.getInvitingFriends().remove(currentUser);

                userRepository.save(friend);
                userRepository.save(currentUser);
            } else {
                throw new EntityDoesNotExistException("This friend request has either not been received or sent or both.");
            }
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
