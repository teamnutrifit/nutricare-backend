package ch.heigvd.nutricare.service;

import ch.heigvd.nutricare.model.message.Message;
import ch.heigvd.nutricare.repository.MessageRepository;
import ch.heigvd.nutricare.repository.UserRepository;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserService userService;

    /**
     * Get all messages sent by the user with the given id
     * @param username id of the user
     * @return a list of messages sent by the given user
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public List<Message> getMessagesFrom(String username) {
        try {
            return messageRepository.findBySenderAndReceptor(userRepository.findByUsername(username).get(), userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Get all messages sent to the user with the given id
     * @param username id of the user
     * @return a list of messages sent to the given user
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public List<Message> getMessagesTo(String username) {
        try {
            return messageRepository.findBySenderAndReceptor(userService.getCurrentUser(), userRepository.findByUsername(username).get());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    /**
     * Send a message. This simply creates a new message in the data base.
     * @param message the message to save in data base
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public void sendMessageTo(String username, Message message) {
        try {
            message.setReceptor(userService.getUserByUsername(username));
            message.setSender(userService.getCurrentUser());
            messageRepository.save(message);
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public List<Message> getMessages() {
        try {
            return messageRepository.findByReceptor(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }

    public List<Message> getMessagesSent() {
        try {
            return messageRepository.findBySender(userService.getCurrentUser());
        } catch (Exception e) {
            throw new InternalErrorException(e.getMessage());
        }
    }
}
