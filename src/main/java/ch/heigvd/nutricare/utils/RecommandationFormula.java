package ch.heigvd.nutricare.utils;

import ch.heigvd.nutricare.model.user.sex.ESex;

public class RecommandationFormula {
    public static double getImcRecommandation(double weight, double height) {
        // BMI : mass (kg) / (height^2)
        try {
            return weight / height * height;
        } catch (Exception e) {
            return 0;
        }
    }

    public static double getWaterRecommandation() {
        return 2;
    }

    public static double getCaloriesRecommandation(ESex sex, double weight, double height, int age) {
        return 0;
    }
}
