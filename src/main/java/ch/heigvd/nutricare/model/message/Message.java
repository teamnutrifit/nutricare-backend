package ch.heigvd.nutricare.model.message;

import ch.heigvd.nutricare.model.user.User;
import lombok.*;
import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMessage;

    @NonNull
    private String title;

    @NonNull
    private String text;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(nullable = false)
    private User sender;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(nullable = false)
    private User receptor;

    @NonNull
    private boolean hasBeenRead;
}
