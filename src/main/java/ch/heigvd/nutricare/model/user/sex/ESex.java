package ch.heigvd.nutricare.model.user.sex;

import ch.heigvd.nutricare.service.exception.InternalErrorException;

public enum ESex {
	FEMALE,
    MALE;

	public static ESex getSex(String sex) {
	    if (sex.equals(MALE.name())) {
	        return MALE;
        } else if (sex.equals(FEMALE.name())) {
	        return FEMALE;
        }
	    throw new InternalErrorException("This sex does not exist");
    }
}
