package ch.heigvd.nutricare.model.user.role;

import ch.heigvd.nutricare.service.exception.InternalErrorException;

public enum ERole {
	ROLE_USER,
    ROLE_ADMIN;

	public static ERole getRole(String role) {
	    if (role.equals(ROLE_ADMIN.name())) {
	        return ROLE_ADMIN;
        } else if (role.equals(ROLE_USER.name())) {
	        return ROLE_USER;
        }
	    throw new InternalErrorException("This role does not exist");
    }
}
