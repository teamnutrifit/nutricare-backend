package ch.heigvd.nutricare.model.user;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import ch.heigvd.nutricare.model.consommation.Recommandation;
import ch.heigvd.nutricare.model.image.Image;
import ch.heigvd.nutricare.model.user.sex.ESex;
import com.fasterxml.jackson.annotation.JsonFormat;

import ch.heigvd.nutricare.model.user.role.Role;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.With;

@Data
@Getter
@Setter
@Builder(toBuilder = true)
@With
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "User")
public class User {
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idUser;

    @NonNull
    @Column(unique = true)
    private String username;

    @NonNull
    private String firstname;

    @NonNull
    private String lastname;

    @NonNull
    @JsonFormat(pattern = "dd-mm-yyyy")
    private LocalDate birthdate;

    @NonNull
    @Column(unique = true)
    private String email;

    @NonNull
    private String password;

    @NonNull
    private ESex sex;

    @ManyToMany(fetch = FetchType.EAGER)
    @Singular
    private Set<Role> roles;

    /**
     * FRIENDSHIPS
     */
    @JsonIgnoreProperties({"roles", "friends", "invitedFriends", "invitingFriends"})
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<User> friends;

    @JsonIgnoreProperties({"roles", "friends", "invitedFriends", "invitingFriends"})
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<User> invitedFriends;

    @JsonIgnoreProperties({"roles", "friends", "invitedFriends", "invitingFriends"})
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<User> invitingFriends;

    /**
     * IMAGE
     */
    @ManyToOne(fetch = FetchType.LAZY/*,
                cascade = CascadeType.ALL*/)
    private Image profilePic;

    /**
     * RECOMMANDATION
     */
    @ManyToOne(fetch = FetchType.LAZY,
                cascade = CascadeType.ALL)
    private Recommandation recommandation;

}
