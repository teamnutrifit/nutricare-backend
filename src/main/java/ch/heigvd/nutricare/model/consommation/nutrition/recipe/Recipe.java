package ch.heigvd.nutricare.model.consommation.nutrition.recipe;

import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.image.Image;
import ch.heigvd.nutricare.model.user.User;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Recipe")
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idRecipe;

    @NonNull
    private String name;

    @ManyToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Category category;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<Instruction> instructions;

    @ManyToOne(fetch = FetchType.LAZY,
                cascade = CascadeType.ALL)
    private User author;

    @ManyToMany(fetch = FetchType.LAZY,
                cascade = CascadeType.ALL)
    private List<User> subscribers;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<FoodIn> foodList;

    @ManyToOne(fetch = FetchType.LAZY,
                cascade = CascadeType.ALL)
    private Image image;
}
