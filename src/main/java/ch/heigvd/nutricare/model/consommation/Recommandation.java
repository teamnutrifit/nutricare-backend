package ch.heigvd.nutricare.model.consommation;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Recommandation")
public class Recommandation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idRecommandation;

    @NonNull
    private double water;

    @NonNull
    private double calories;

    @NonNull
    private double imc;
}
