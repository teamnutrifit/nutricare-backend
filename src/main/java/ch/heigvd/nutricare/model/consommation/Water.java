package ch.heigvd.nutricare.model.consommation;

import ch.heigvd.nutricare.model.user.User;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Water")
public class Water {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idWater;

    @NonNull
    private double quantity;

    @NonNull
    private LocalDate dateOfConsumption;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private User user;
}
