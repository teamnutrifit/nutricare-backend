package ch.heigvd.nutricare.model.consommation.nutrition;

import ch.heigvd.nutricare.model.consommation.nutrition.utils.EUnit;
import lombok.*;

import javax.persistence.*;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "FoodIn")
public class FoodIn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idFoodIn;

    @NonNull
    private EUnit unit;

    @NonNull
    private double quantity;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Food food;
}
