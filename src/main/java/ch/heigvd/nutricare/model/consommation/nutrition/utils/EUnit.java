package ch.heigvd.nutricare.model.consommation.nutrition.utils;

import ch.heigvd.nutricare.service.exception.InternalErrorException;

public enum EUnit {
    GRAMS("g"),
    KILO_GRAMS("Kg"),
    LITER("L"),
    TEA_SPOON("tea spoon(s)"),
    TABLE_SPOON("table spoon(s)"),
    PINCH("pinch"),
    QUANTITY("");

    private String asString;

    EUnit(String asString) {
        this.asString = asString;
    }

    @Override
    public String toString() {
        return asString;
    }

    public static EUnit getUnit(String unit) {
        if (unit.equals(GRAMS.toString())) {
            return GRAMS;
        } else if (unit.equals(KILO_GRAMS.toString())) {
            return KILO_GRAMS;
        } else if (unit.equals(LITER.toString())) {
            return LITER;
        } else if (unit.equals(QUANTITY.toString())) {
            return QUANTITY;
        } else if (unit.equals(TEA_SPOON.toString())) {
            return TEA_SPOON;
        } else if (unit.equals(TABLE_SPOON.toString())) {
            return TABLE_SPOON;
        } else if (unit.equals(PINCH.toString())) {
            return PINCH;
        }
        throw new InternalErrorException("This unit does not exist");
    }
}
