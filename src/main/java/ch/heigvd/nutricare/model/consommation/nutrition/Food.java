package ch.heigvd.nutricare.model.consommation.nutrition;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity(name = "Food")
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idFood;

    @NonNull
    private String name;

    @NonNull
    private String description;

    @NonNull
    private double carbohydrate;

    @NonNull
    private double sugar;

    @NonNull
    private double fat;

    @NonNull
    private double saturated;

    @NonNull
    private double protein;
}
