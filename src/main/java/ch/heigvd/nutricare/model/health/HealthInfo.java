package ch.heigvd.nutricare.model.health;

import ch.heigvd.nutricare.model.user.User;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "HealthInfo")
public class HealthInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idHealthInfo;

    private String goal;

    private String activityLevel;

    private double height;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<Weight> weightEntities;

    @NonNull
    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private User user;

}
