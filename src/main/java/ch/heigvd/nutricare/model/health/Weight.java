package ch.heigvd.nutricare.model.health;

import ch.heigvd.nutricare.dto.WeightDTO;
import org.openapitools.jackson.nullable.JsonNullable;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Weight")
public class Weight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idWeight;

    private double weight;

    @JsonFormat(pattern="dd-mm-yyyy")
    private LocalDate weightDate;
}
