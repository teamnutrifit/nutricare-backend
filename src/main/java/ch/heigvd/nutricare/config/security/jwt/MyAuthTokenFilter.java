package ch.heigvd.nutricare.config.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.heigvd.nutricare.service.exception.WrongCredentialsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import ch.heigvd.nutricare.service.TokenBlacklistService;
import ch.heigvd.nutricare.service.UserService;

public class MyAuthTokenFilter extends OncePerRequestFilter {
	@Autowired
	private JwtUtils jwtUtils;

	@Autowired
	private UserService userService;

	@Autowired
	private TokenBlacklistService tokenBlacklistService;

	private static final Logger logger = LoggerFactory.getLogger(MyAuthTokenFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			String jwt = jwtUtils.getTokenFromHeader(getAuthorizationHeader(request));
			if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
				String username = jwtUtils.getUserNameFromJwtToken(jwt);

				UserDetails userDetails = userService.loadUserByUsername(username);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, jwt, userDetails.getAuthorities());

				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

				// If the token is not blacklisted then we can add the auth to the context.
				if (tokenBlacklistService.get(jwt).isEmpty()) {
					SecurityContextHolder.getContext().setAuthentication(authentication);
				} else {
					throw new WrongCredentialsException("The given token is black listed");
				}
			} else if(protectedEndpoint(request)) {
				throw new WrongCredentialsException("No JWT was given");
			}
		} catch (WrongCredentialsException e) {
			logger.error("Wrong credentials {}", e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Cannot set user authentication: {}", e.getMessage());
		}

		filterChain.doFilter(request, response);
	}

	private boolean protectedEndpoint(HttpServletRequest request) {
		return  request.getRequestURL().toString().contains("/logout") 		||
				request.getRequestURL().toString().contains("/categories") 	||
				request.getRequestURL().toString().contains("/health-info") ||
				request.getRequestURL().toString().contains("/images") 		||
				request.getRequestURL().toString().contains("/messages") 	||
				request.getRequestURL().toString().contains("/foods") 		||
				request.getRequestURL().toString().contains("/meals") 		||
				request.getRequestURL().toString().contains("/recipes") 	||
				request.getRequestURL().toString().contains("/users") 		||
				request.getRequestURL().toString().contains("/waters") 		||
				request.getRequestURL().toString().contains("/weights");
	}

	/**
	 * Get the authorization header of the given http request
	 * @param request the request from which to get the authorization header
	 * @return the authorization header as a string
	 * @author Alexandre Simik
	 */
	private String getAuthorizationHeader(HttpServletRequest request) {
		return request.getHeader("Authorization");
	}
}
