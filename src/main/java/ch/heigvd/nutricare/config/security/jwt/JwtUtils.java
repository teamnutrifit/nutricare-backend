package ch.heigvd.nutricare.config.security.jwt;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import ch.heigvd.nutricare.config.security.model.UserDetailsImpl;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;

import org.springframework.util.StringUtils;

@Slf4j
@Component
public class JwtUtils {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${nutricare.app.jwtSecret}")
	private String jwtSecret;

	@Value("${nutricare.app.jwtExpirationMs}")
	private int jwtExpirationMs;

	/**
	 * Generate JWT token for the given authentication
	 * @param authentication the authentication for which to create a JWT token
	 * @return the JWT created for the authentication
	 * @author Alexandre Simik
	 */
	public String generateJwtToken(Authentication authentication) {

		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

		return Jwts.builder()
				.setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}

	/**
	 * Find the linked user name for a JWT token
	 * @param token the token for which to find the user
	 * @return the username linked to the given token
	 * @author Alexandre Simik
	 */
	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

	/**
	 * Validate the given authentication token in order to check if it is correct
	 * @param authToken the token to validate
	 * @return true if the token is correct
	 * @author Alexandre Simik
	 */
	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			log.warn("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			log.warn("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			log.warn("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			log.warn("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			log.warn("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}

	/**
	 * Find out the expiration time of the given token
	 * @param token the token for which to get the expiration time
	 * @return the remaining time to live
	 * @author Alexandre Simik
	 */
	public long getExpirationTime(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getExpiration().getTime();
	}

	/**
	 * This simply removes the "Bearer " and returns the end of the header (the JWT).
	 * @author Alexandre Simik
	 */
	public String getTokenFromHeader(String header){
		if (StringUtils.hasText(header) && header.startsWith("Bearer ")) {
			return header.substring(7);
		}
		return null;
	}
}
