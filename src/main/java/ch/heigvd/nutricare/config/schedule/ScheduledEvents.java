package ch.heigvd.nutricare.config.schedule;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import ch.heigvd.nutricare.service.RoleService;
import ch.heigvd.nutricare.service.TokenBlacklistService;
import ch.heigvd.nutricare.service.UserService;

/**
 * This class is used to enable the scheduled events. By convention we put all
 * scheduled events here.
 */
@Configuration
@EnableScheduling
public class ScheduledEvents {

    @Autowired
    private TokenBlacklistService tokenBlacklistService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    /**
     * Delete the black listed tokens
     * @author Alexandre Simik
     */
    @Scheduled(fixedDelayString = "${nutricare.schedule.TokenBlacklistClean}")
    public void deleteBlackListedTokens() {
        tokenBlacklistService.deleteExpired();
        logger.info("Delete the black listed tokens.");
    }

    /**
     * Methods to run after application starts up
     * @author Alexandre Simik
     */
    @PostConstruct
    public void runAfterStartup() {
        roleService.generateRolesToDatabase();
        logger.info("Generate roles to database.");
        userService.createMainAdminUser();
        logger.info("Created administrator.");
    }
}
