package ch.heigvd.nutricare.controller.api;

import ch.heigvd.nutricare.api.ImageApi;
import ch.heigvd.nutricare.dto.*;
import ch.heigvd.nutricare.mapper.ImageMapper;
import ch.heigvd.nutricare.mapper.RecipeMapper;
import ch.heigvd.nutricare.mapper.UserMapper;
import ch.heigvd.nutricare.service.ImageService;
import ch.heigvd.nutricare.service.RecipeService;
import ch.heigvd.nutricare.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;

@Api(tags="Image", description = "All endpoints used to add, modify and remove images in the app.")
@Controller
public class ImageController implements ImageApi {
    @Autowired
    private ImageService imageService;
    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private RecipeMapper recipeMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private RecipeService recipeService;

    @Override
    public ResponseEntity<ImageDTO> createImageUser(@Valid String body) {
        ImageCreationDTO i = new ImageCreationDTO();
        i.image(body);
        return ResponseEntity.ok(imageMapper.toDto(imageService.createOrUpdateImageUser(imageMapper.toModelFromCreation(i))));
    }

    @Override
    public ResponseEntity<ImageDTO> createImageRecipe(String name, @Valid String body) {
        ImageCreationDTO i = new ImageCreationDTO();
        i.image(body);
        return ResponseEntity.ok(imageMapper.toDto(imageService.createOrUpdateImageRecipe(name, imageMapper.toModelFromCreation(i))));
    }

    @Override
    public ResponseEntity<RecipeDTO> deleteImageRecipe(String name) {
        return ResponseEntity.ok(recipeMapper.toDto(imageService.deleteImageRecipe(name)));
    }

    @Override
    public ResponseEntity<UserSimpleDTO> deleteImageUser() {
        return ResponseEntity.ok(userMapper.toSimple(imageService.deleteImageUser()));
    }

    @Override
    public ResponseEntity<ImageDTO> getCurrentUserImage() {
        return ResponseEntity.ok(imageMapper.toDto(imageService.getCurrentUserImage()));
    }

    @Override
    public ResponseEntity<ImageDTO> getImageUser(String username) {
        return ResponseEntity.ok(imageMapper.toDto(imageService.getUserImage(username)));
    }

    @Override
    public ResponseEntity<ImageDTO> updateImageRecipe(String name, @Valid String body) {
        ImageDTO i = new ImageDTO();
        i.setId((int)recipeService.getRecipe(name).getImage().getIdImage());
        i.image(body);
        return ResponseEntity.ok(imageMapper.toDto(imageService.createOrUpdateImageRecipe(name, imageMapper.toModel(i))));
    }

    @Override
    public ResponseEntity<ImageDTO> updateImageUser(@Valid String body) {
        ImageDTO i = new ImageDTO();
        i.setId((int)userService.getCurrentUser().getProfilePic().getIdImage());
        i.image(body);
        return ResponseEntity.ok(imageMapper.toDto(imageService.createOrUpdateImageUser(imageMapper.toModel(i))));
    }
}
