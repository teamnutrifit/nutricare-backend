package ch.heigvd.nutricare.controller.api;

import javax.validation.Valid;

import ch.heigvd.nutricare.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import ch.heigvd.nutricare.api.AuthenticationApi;
import ch.heigvd.nutricare.controller.util.ApiHelper;
import ch.heigvd.nutricare.dto.ApiMessageDTO;
import ch.heigvd.nutricare.dto.LoginRequestDTO;
import ch.heigvd.nutricare.dto.LoginSuccessDTO;
import ch.heigvd.nutricare.dto.RegisterDTO;
import ch.heigvd.nutricare.mapper.UserMapper;
import ch.heigvd.nutricare.service.UserService;
import ch.heigvd.nutricare.service.exception.IncompleteBodyException;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Api(tags = "Authentication", description = "All endpoints used for authentication and registration.")
@RestController
@Slf4j
public class AuthController implements AuthenticationApi {

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseEntity<ApiMessageDTO> register(@Valid RegisterDTO registerDTO) {
        try {
            User user = userMapper.toModel(registerDTO);
            userService.register(user);
        } catch (NullPointerException e) {
            throw new IncompleteBodyException();
        }
        return ResponseEntity.ok(ApiHelper.created("Register successful"));
    }

    @Override
    public ResponseEntity<LoginSuccessDTO> login(@Valid LoginRequestDTO loginRequestDTO) {
        String jwt = userService.login(loginRequestDTO.getUsernameOrEmail(), loginRequestDTO.getPassword());
        User user = userService.getCurrentUser();

        LoginSuccessDTO loginSuccessDTO = userMapper.toLoginSuccessDTO(user, jwt);

        return ResponseEntity.ok(loginSuccessDTO);
    }

    @Override
    public ResponseEntity<ApiMessageDTO> logout() {
        userService.logout();
        return ResponseEntity.ok(ApiHelper.ok("Logout successful"));
    }
}
