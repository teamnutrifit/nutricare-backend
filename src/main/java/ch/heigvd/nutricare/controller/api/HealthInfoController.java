package ch.heigvd.nutricare.controller.api;

import javax.validation.Valid;

import ch.heigvd.nutricare.dto.HealthInfoCreationDTO;
import ch.heigvd.nutricare.dto.HealthInfoModificationDTO;
import ch.heigvd.nutricare.mapper.HealthInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import ch.heigvd.nutricare.api.HealthInformationApi;
import ch.heigvd.nutricare.controller.util.ApiHelper;
import ch.heigvd.nutricare.dto.ApiMessageDTO;
import ch.heigvd.nutricare.dto.HealthInfoDTO;
import ch.heigvd.nutricare.service.HealthInfoService;
import io.swagger.annotations.Api;

@Api(tags="Health Information", description = "All endpoints used to add, modify and remove user's health information.")
@Controller
public class HealthInfoController implements HealthInformationApi {
    @Autowired
    private HealthInfoService healthInfoService;

    @Autowired
    private HealthInfoMapper healthInfoMapper;

    @Override
    public ResponseEntity<HealthInfoDTO> getUserHealth() {
        return ResponseEntity.ok(healthInfoMapper.toDto(healthInfoService.getHealthInfo()));
    }

    @Override
    public ResponseEntity<HealthInfoDTO> updateHealthInfo(@Valid HealthInfoModificationDTO healthInfoDTO) {
        return ResponseEntity.ok(healthInfoMapper.toDto(
                healthInfoService.updateOrCreateHealthInfo(healthInfoMapper.toModelFromModification(healthInfoDTO))
        ));
    }

    @Override
    public ResponseEntity<HealthInfoDTO> createHealthInfo(@Valid HealthInfoCreationDTO healthInfoCreationDTO) {
        return ResponseEntity.ok(healthInfoMapper.toDto(
                healthInfoService.updateOrCreateHealthInfo(healthInfoMapper.toModelFromCreation(healthInfoCreationDTO))
        ));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteHealthInfo() {
        healthInfoService.delete();
        return ResponseEntity.ok(ApiHelper.ok("Health info deleted successfully"));
    }
}
