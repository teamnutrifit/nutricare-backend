package ch.heigvd.nutricare.controller.api;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import ch.heigvd.nutricare.dto.*;
import ch.heigvd.nutricare.mapper.ImageMapper;
import ch.heigvd.nutricare.mapper.UserMapper;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import ch.heigvd.nutricare.api.UserApi;
import ch.heigvd.nutricare.controller.util.ApiHelper;
import ch.heigvd.nutricare.service.UserService;
import io.swagger.annotations.Api;

@Api(tags="User", description = "All endpoints used for user modification.")
@Controller
public class UserController implements UserApi {
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseEntity<Void> acceptFriend(String username) {
        userService.acceptFriend(username);
        return null; // TODO
    }

    @Override
    public ResponseEntity<Void> inviteFriend(String username) {
        userService.inviteFriend(username);
        return null; // TODO
    }

    @Override
    public ResponseEntity<Void> removeFriend(String username) {
        userService.removeFriend(username);
        return null; // TODO
    }

    @Override
    public ResponseEntity<Void> cancelFriendRequest(String username) {
        userService.cancelFriendRequest(username);
        return null; // TODO
    }

    @Override
    public ResponseEntity<UserDTO> getUser(String username) {
        return ResponseEntity.ok(userMapper.toDto(userService.getUserByUsername(username)));
    }

    @Override
    public ResponseEntity<List<UserSimpleDTO>> getUsers() {
        Iterable<User> users = userService.getAllUsers();
        List<User> u = new LinkedList<>();
        users.forEach(u::add);
        return ResponseEntity.ok(getUserSimpleDTOS(u));
    }

    @Override
    public ResponseEntity<UserDTO> updateUser(String username, @Valid UserModificationDTO userModificationDTO) {
        return ResponseEntity.ok(userMapper.toDto(userService.updateUser(userMapper.toModelFromModification(userModificationDTO))));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteUser(String username) {
        userService.deleteUserByUsername(username);
        return ResponseEntity.ok(ApiHelper.ok("User deleted successfully."));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> deleteUsers() {
        userService.deleteUsers();
        return ResponseEntity.ok(ApiHelper.ok("All users deleted successfully."));
    }

    @Override
    public ResponseEntity<List<UserSimpleDTO>> getFriends() {
        List<User> users = userService.getFriends();
        return ResponseEntity.ok(getUserSimpleDTOS(users));
    }

    private List<UserSimpleDTO> getUserSimpleDTOS(List<User> userEntities) {
        List<UserSimpleDTO> userSimpleDTOS = new LinkedList<>();
        userEntities.forEach(m -> {
            userSimpleDTOS.add(userMapper.toSimple(m));
        });
        return userSimpleDTOS;
    }

    private List<UserDTO> getUserDTOS(List<User> userEntities) {
        List<UserDTO> dtos = new LinkedList<>();
        userEntities.forEach(m -> {
            dtos.add(userMapper.toDto(m));
        });
        return dtos;
    }
}
