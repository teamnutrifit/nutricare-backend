package ch.heigvd.nutricare.controller.api;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import ch.heigvd.nutricare.dto.WaterModificationDTO;
import ch.heigvd.nutricare.mapper.WaterMapper;
import ch.heigvd.nutricare.model.consommation.Water;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import ch.heigvd.nutricare.api.WaterApi;
import ch.heigvd.nutricare.controller.util.ApiHelper;
import ch.heigvd.nutricare.dto.ApiMessageDTO;
import ch.heigvd.nutricare.dto.WaterDTO;
import ch.heigvd.nutricare.service.UserService;
import ch.heigvd.nutricare.service.WaterService;
import io.swagger.annotations.Api;

@Api(tags="Water", description = "All endpoints used to add and get user's water consumption.")
@Controller
public class WaterController implements WaterApi {
    @Autowired
    private WaterService waterService;

    @Autowired
    private WaterMapper waterMapper;


    @Override
    public ResponseEntity<WaterDTO> getUserWaterDay(String date) {
        LocalDate localD = LocalDate.parse(date);
        return ResponseEntity.of(Optional.of(waterMapper.toDto(waterService.getFromDay(localD))));
    }

    @Override
    public ResponseEntity<List<WaterDTO>> getUserWater() {
        List<Water> waterEntities = waterService.getWater();
        return ResponseEntity.of(Optional.of(getWaterDTOS(waterEntities)));
    }

    @Override
    public ResponseEntity<List<WaterDTO>> getUserWaterFromDays(Integer dayNbr) {
        List<Water> waterEntities = waterService.getFromDays(dayNbr);
        return ResponseEntity.of(Optional.of(getWaterDTOS(waterEntities)));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> modifyWater(@Valid WaterModificationDTO waterDTO) {
        waterService.save(waterMapper.toModelFromModification(waterDTO));
        return ResponseEntity.ok(ApiHelper.created("Hydratation modified successfully."));
    }

    private List<WaterDTO> getWaterDTOS(List<Water> waterEntities) {
        List<WaterDTO> dtos = new LinkedList<>();

        waterEntities.forEach(e -> {
            dtos.add(waterMapper.toDto(e));
        });
        return dtos;
    }
}
