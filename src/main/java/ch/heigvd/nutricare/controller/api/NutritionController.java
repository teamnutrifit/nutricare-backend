package ch.heigvd.nutricare.controller.api;

import ch.heigvd.nutricare.api.NutritionApi;
import ch.heigvd.nutricare.dto.*;
import ch.heigvd.nutricare.mapper.FoodInMapper;
import ch.heigvd.nutricare.mapper.FoodMapper;
import ch.heigvd.nutricare.mapper.MealMapper;
import ch.heigvd.nutricare.model.consommation.nutrition.Food;
import ch.heigvd.nutricare.model.consommation.nutrition.Meal;
import ch.heigvd.nutricare.service.FoodService;
import ch.heigvd.nutricare.service.MealService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Api(
    tags="Nutrition",
    description = "All endpoints used to add, modify and remove elements linked to the nutrition part of the app.")
@Controller
public class NutritionController implements NutritionApi {
    @Autowired
    private FoodMapper foodMapper;

    @Autowired
    private FoodInMapper foodInMapper;

    @Autowired
    private MealMapper mealMapper;

    @Autowired
    private FoodService foodService;

    @Autowired
    private MealService mealService;

    @Override
    public ResponseEntity<FoodDTO> createFood(@Valid FoodCreationDTO foodCreationDTO) {
        return ResponseEntity.ok(foodMapper.toDto(foodService.createOrSaveFood(foodMapper.toModelFromCreation(foodCreationDTO))));
    }

    @Override
    public ResponseEntity<MealDTO> createMeal(@Valid MealCreationDTO mealCreationDTO) {
        return ResponseEntity.ok(mealMapper.toDto(mealService.createOrSaveMeal(mealCreationDTO.getName(), mealMapper.toModelFromCreation(mealCreationDTO))));
    }

    @Override
    public ResponseEntity<List<FoodDTO>> getFood() {
        Iterable<Food> foodList = foodService.getFood();
        List<FoodDTO> foodDTOList = new LinkedList<>();
        foodList.forEach(food -> {
            foodDTOList.add(foodMapper.toDto(food));
        });
        return ResponseEntity.ok(foodDTOList);
    }

    @Override
    public ResponseEntity<Void> deleteMeal(Integer id) {
        mealService.deleteMeal(id);
        return null;
    }

    @Override
    public ResponseEntity<FoodDTO> getFoodByName(String name) {
        return ResponseEntity.ok(foodMapper.toDto(foodService.getFoodByName(name)));
    }

    @Override
    public ResponseEntity<FoodDTO> updateFood(@Valid FoodModificationDTO foodModificationDTO) {
        return ResponseEntity.ok(foodMapper.toDto(foodService.createOrSaveFood(foodMapper.toModelFromModification(foodModificationDTO))));
    }

    @Override
    public ResponseEntity<MealDTO> updateMeal(Integer id, @Valid MealCreationDTO mealCreationDTO) {
        return ResponseEntity.ok(mealMapper.toDto(mealService.updateMeal(id, mealMapper.toModelFromCreation(mealCreationDTO))));
    }

    @Override
    public ResponseEntity<List<MealDTO>> getMealsDate(String date) {
        List<Meal> mealsDate = mealService.getMealsDate(date);
        List<MealDTO> mealDTOS = new LinkedList<>();
        mealsDate.forEach(meal -> {
            mealDTOS.add(mealMapper.toDto(meal));
        });
        return ResponseEntity.ok(mealDTOS);
    }

    @Override
    public ResponseEntity<List<MealDTO>> getMeals() {
        List<Meal> mealList = mealService.getMeals();
        List<MealDTO> mealDTOList = new LinkedList<>();
        mealList.forEach(meal -> {
            mealDTOList.add(mealMapper.toDto(meal));
        });
        return ResponseEntity.ok(mealDTOList);
    }

    @Override
    public ResponseEntity<MealDTO> addFood(Integer id, @Valid FoodInCreationDTO foodInCreationDTO) {
        return ResponseEntity.ok(mealMapper.toDto(mealService.addFood(id, foodInMapper.toModelFromCreation(foodInCreationDTO))));
    }

    @Override
    public ResponseEntity<List<FoodDTO>> searchFood(String keyword) {
        Iterable<Food> foodList = foodService.searchFood(keyword);
        List<FoodDTO> foodDTOList = new LinkedList<>();
        foodList.forEach(food -> {
            foodDTOList.add(foodMapper.toDto(food));
        });
        return ResponseEntity.ok(foodDTOList);
    }

}

