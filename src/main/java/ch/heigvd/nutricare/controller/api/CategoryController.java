package ch.heigvd.nutricare.controller.api;

import ch.heigvd.nutricare.api.CategoryApi;
import ch.heigvd.nutricare.dto.CategoryCreationDTO;
import ch.heigvd.nutricare.dto.CategoryDTO;
import ch.heigvd.nutricare.dto.CategoryModificationDTO;
import ch.heigvd.nutricare.mapper.CategoryMapper;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Category;
import ch.heigvd.nutricare.service.CategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Api(tags="Category", description = "All endpoints used to manipulate categories.")
@Controller
public class CategoryController implements CategoryApi {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryService categoryService;

    @Override
    public ResponseEntity<CategoryDTO> createCategory(@Valid CategoryCreationDTO categoryCreationDTO) {
        return ResponseEntity.ok(categoryMapper.toDto(categoryService.createOrSaveCategory(categoryCreationDTO.getName(), categoryMapper.toModelFromCreation(categoryCreationDTO))));
    }

    @Override
    public ResponseEntity<CategoryDTO> updateCategory(String name, @Valid CategoryModificationDTO categoryDTO) {
        return ResponseEntity.ok(categoryMapper.toDto(categoryService.createOrSaveCategory(name, categoryMapper.toModelFromModification(categoryDTO))));
    }

    @Override
    public ResponseEntity<List<CategoryDTO>> getCategories() {
        List<Category> categories = categoryService.getCategories();
        List<CategoryDTO> categoryDTOList = new LinkedList<>();
        categories.forEach(category -> {
            categoryDTOList.add(categoryMapper.toDto(category));
        });
        return ResponseEntity.ok(categoryDTOList);
    }

    @Override
    public ResponseEntity<CategoryDTO> getCategory(String name) {
        return ResponseEntity.ok(categoryMapper.toDto(categoryService.getCategory(name)));
    }

}
