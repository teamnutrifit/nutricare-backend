package ch.heigvd.nutricare.controller.api;

import ch.heigvd.nutricare.api.RecipeApi;
import ch.heigvd.nutricare.dto.FoodInCreationDTO;
import ch.heigvd.nutricare.dto.InstructionCreationDTO;
import ch.heigvd.nutricare.dto.RecipeCreationDTO;
import ch.heigvd.nutricare.dto.RecipeDTO;
import ch.heigvd.nutricare.mapper.FoodInMapper;
import ch.heigvd.nutricare.mapper.InstructionMapper;
import ch.heigvd.nutricare.mapper.RecipeMapper;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Recipe;
import ch.heigvd.nutricare.service.RecipeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;
@Api(
        tags="Recipe",
        description = "All endpoints used to add, modify and remove recipes in the app.")
@Controller
public class RecipeController implements RecipeApi {

    @Autowired
    private InstructionMapper instructionMapper;

    @Autowired
    private FoodInMapper foodInMapper;

    @Autowired
    private RecipeMapper recipeMapper;

    @Autowired
    private RecipeService recipeService;

    @Override
    public ResponseEntity<RecipeDTO> updateRecipe(String name, @Valid RecipeDTO recipeDTO) {
        return ResponseEntity.ok(recipeMapper.toDto(recipeService.createOrSaveRecipe(name, recipeMapper.toModel(recipeDTO))));
    }

    @Override
    public ResponseEntity<List<RecipeDTO>> getRecipesOfCategory(String categoryName) {
        List<RecipeDTO> recipes = new LinkedList<>();
        List<Recipe> r = recipeService.getRecipes(categoryName);
        r.forEach(recipe -> {
            recipes.add(recipeMapper.toDto(recipe));
        });
        return ResponseEntity.ok(recipes);
    }

    @Override
    public ResponseEntity<RecipeDTO> getRecipe(String name) {
        return ResponseEntity.ok(recipeMapper.toDto(recipeService.getRecipe(name)));
    }

    @Override
    public ResponseEntity<List<RecipeDTO>> getRecipes() {
        List<Recipe> recipes = recipeService.getAll();
        List<RecipeDTO> recipeDTOS = new LinkedList<>();
        recipes.forEach(recipe -> {
            recipeDTOS.add(recipeMapper.toDto(recipe));
        });
        return ResponseEntity.ok(recipeDTOS);
    }

    @Override
    public ResponseEntity<RecipeDTO> createRecipe(@Valid RecipeCreationDTO recipeCreationDTO) {
        return ResponseEntity.ok(recipeMapper.toDto(recipeService.createOrSaveRecipe(recipeCreationDTO.getName(), recipeMapper.toModelFromCreation(recipeCreationDTO))));
    }

    @Override
    public ResponseEntity<RecipeDTO> addInstruction(String name, @Valid InstructionCreationDTO instructionCreationDTO) {
        return ResponseEntity.ok(recipeMapper.toDto(recipeService.addInstruction(name, instructionMapper.toModelFromCreation(instructionCreationDTO))));
    }

    @Override
    public ResponseEntity<RecipeDTO> addFoodRecipe(String name, @Valid FoodInCreationDTO foodInCreationDTO) {
        return ResponseEntity.ok(recipeMapper.toDto(recipeService.addFood(name, foodInMapper.toModelFromCreation(foodInCreationDTO))));
    }

    @Override
    public ResponseEntity<RecipeDTO> subscribeToRecipe(String name) {
        return ResponseEntity.ok(recipeMapper.toDto(recipeService.subscribeTo(name)));
    }
}
