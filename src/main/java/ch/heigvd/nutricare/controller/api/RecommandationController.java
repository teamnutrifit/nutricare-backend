package ch.heigvd.nutricare.controller.api;

import ch.heigvd.nutricare.api.RecommandationApi;
import ch.heigvd.nutricare.dto.RecommandationDTO;
import ch.heigvd.nutricare.mapper.RecommandationMapper;
import ch.heigvd.nutricare.service.RecommandationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;

@Api(
        tags="Recommandation",
        description = "All endpoints used to add, modify and remove recommandations in the app.")
@Controller
public class RecommandationController implements RecommandationApi {
    @Autowired
    private RecommandationService recommandationService;

    @Autowired
    private RecommandationMapper recommandationMapper;

    @Override
    public ResponseEntity<RecommandationDTO> getRecommandation() {
        return ResponseEntity.ok(recommandationMapper.toDto(recommandationService.getRecommandation()));
    }

    @Override
    public ResponseEntity<RecommandationDTO> updateRecommandation(@Valid RecommandationDTO recommandationDTO) {
        return ResponseEntity.ok(recommandationMapper.toDto(recommandationService.updateRecommandation(recommandationMapper.toModel(recommandationDTO))));
    }
}
