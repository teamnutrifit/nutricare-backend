package ch.heigvd.nutricare.controller.api;

import ch.heigvd.nutricare.api.WeightApi;
import ch.heigvd.nutricare.dto.WeightCreationDTO;
import ch.heigvd.nutricare.dto.WeightDTO;
import ch.heigvd.nutricare.mapper.WeightMapper;
import ch.heigvd.nutricare.model.health.Weight;
import ch.heigvd.nutricare.service.HealthInfoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Api(tags="Weight", description = "All endpoints used to add, modify and remove user's weight information.")
@Controller
public class WeightController implements WeightApi {
    @Autowired
    private HealthInfoService healthInfoService;

    @Autowired
    private WeightMapper weightMapper;

    @Override
    public ResponseEntity<List<WeightDTO>> getUserWeights() {
        List<Weight> weightEntities = healthInfoService.getWeights();
        return ResponseEntity.ok(getWeightDTOS(weightEntities));
    }

    @Override
    public ResponseEntity<List<WeightDTO>> getUserWeightsDays(Integer dayNbr) {
        LocalDate oldestDate = LocalDate.now().minusDays(dayNbr);
        return ResponseEntity.ok(getWeightDTOS(healthInfoService.getWeightsByDate(oldestDate)));
    }

    @Override
    public ResponseEntity<List<WeightDTO>> getUserWeightsMonths(Integer monthNbr) {
        LocalDate oldestDate = LocalDate.now().minusMonths(monthNbr);
        return ResponseEntity.ok(getWeightDTOS(healthInfoService.getWeightsByDate(oldestDate)));
    }

    @Override
    public ResponseEntity<WeightDTO> getUserLastWeight() {
        return ResponseEntity.ok(weightMapper.toDto(healthInfoService.getLastWeight()));
    }

    @Override
    public ResponseEntity<WeightDTO> updateWeight(@Valid WeightDTO weightDTO) {
        return ResponseEntity.ok(weightMapper.toDto(healthInfoService.updateOrCreateWeight(weightMapper.toModel(weightDTO))));
    }

    @Override
    public ResponseEntity<WeightDTO> createWeight(@Valid WeightCreationDTO weightCreationDTO) {
        return ResponseEntity.ok(weightMapper.toDto(healthInfoService.updateOrCreateWeight(weightMapper.toModelFromCreation(weightCreationDTO))));
    }

    private List<WeightDTO> getWeightDTOS(List<Weight> weightEntities) {
        List<WeightDTO> dtos = new LinkedList<>();
        weightEntities.forEach(m -> {
            dtos.add(weightMapper.toDto(m));
        });
        return dtos;
    }
}
