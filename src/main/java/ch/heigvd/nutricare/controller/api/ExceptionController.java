package ch.heigvd.nutricare.controller.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import ch.heigvd.nutricare.controller.util.ApiHelper;
import ch.heigvd.nutricare.dto.ApiMessageDTO;
import ch.heigvd.nutricare.service.exception.EntityAlreadyExistException;
import ch.heigvd.nutricare.service.exception.EntityDoesNotExistException;
import ch.heigvd.nutricare.service.exception.IncompleteBodyException;
import ch.heigvd.nutricare.service.exception.InternalErrorException;
import ch.heigvd.nutricare.service.exception.WrongCredentialsException;
import springfox.documentation.annotations.ApiIgnore;

/**
 * This controller handles every custom exception that is thrown in any other
 * Controller. We use this to have faster development time. Simply throw an
 * exception instead of writing a "error response" each time.
 *
 * Source:
 * https://www.tutorialspoint.com/spring_boot/spring_boot_exception_handling.htm
 */
@ControllerAdvice
@ApiIgnore
public class ExceptionController {

    /* All Exceptions below are our own custom exceptions. */

    @ExceptionHandler(value = IncompleteBodyException.class)
    public ResponseEntity<ApiMessageDTO> exception(IncompleteBodyException exception) {
        return ResponseEntity.badRequest().body(ApiHelper.error(ApiHelper.BAD_REQUEST, exception.getMessage()));
    }

    @ExceptionHandler(value = EntityAlreadyExistException.class)
    public ResponseEntity<ApiMessageDTO> exception(EntityAlreadyExistException exception) {
        return ResponseEntity.badRequest().body(ApiHelper.error(ApiHelper.BAD_REQUEST, exception.getMessage()));
    }

    @ExceptionHandler(value = WrongCredentialsException.class)
    public ResponseEntity<ApiMessageDTO> exception(WrongCredentialsException exception) {
        return ResponseEntity.badRequest().body(ApiHelper.error(ApiHelper.BAD_REQUEST, exception.getMessage()));
    }

    @ExceptionHandler(value = InternalErrorException.class)
    public ResponseEntity<ApiMessageDTO> exception(InternalErrorException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ApiHelper.error(ApiHelper.INTERNAL_ERROR, exception.getMessage()));
    }

    @ExceptionHandler(value = EntityDoesNotExistException.class)
    public ResponseEntity<ApiMessageDTO> exception(EntityDoesNotExistException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(ApiHelper.error(ApiHelper.NOT_FOUND, exception.getMessage()));
    }

    /* All exceptions below are Spring Exceptions */

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<ApiMessageDTO> exception(HttpMessageNotReadableException exception) {
        return ResponseEntity.badRequest().body(ApiHelper.error(ApiHelper.BAD_REQUEST, "Cannot parse malformed JSON payload !"));
    }

}
