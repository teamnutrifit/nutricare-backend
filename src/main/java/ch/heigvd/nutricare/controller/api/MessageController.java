package ch.heigvd.nutricare.controller.api;

import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import ch.heigvd.nutricare.dto.MessageCreationDTO;
import ch.heigvd.nutricare.mapper.MessageMapper;
import ch.heigvd.nutricare.model.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import ch.heigvd.nutricare.api.MessageApi;
import ch.heigvd.nutricare.controller.util.ApiHelper;
import ch.heigvd.nutricare.dto.ApiMessageDTO;
import ch.heigvd.nutricare.dto.MessageDTO;
import ch.heigvd.nutricare.service.MessageService;
import io.swagger.annotations.Api;

@Api(tags="Message", description = "All endpoints used to add, modify and remove user's messages.")
@Controller
public class MessageController implements MessageApi {
    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageMapper messageMapper;

    @Override
    public ResponseEntity<List<MessageDTO>> getMessagesSent() {
        List<Message> messageEntities = messageService.getMessagesSent();
        return ResponseEntity.ok(getMessageDTOS(messageEntities));
    }

    @Override
    public ResponseEntity<List<MessageDTO>> getMessages() {
        List<Message> messageEntities = messageService.getMessages();
        return ResponseEntity.ok(getMessageDTOS(messageEntities));
    }

    @Override
    public ResponseEntity<List<MessageDTO>> getMessagesFrom(String username) {
        List<Message> messageEntities = messageService.getMessagesFrom(username);
        return ResponseEntity.ok(getMessageDTOS(messageEntities));
    }

    @Override
    public ResponseEntity<List<MessageDTO>> getMessagesTo(String username) {
        List<Message> messageEntities = messageService.getMessagesTo(username);
        return ResponseEntity.ok(getMessageDTOS(messageEntities));
    }

    @Override
    public ResponseEntity<ApiMessageDTO> sendMessageTo(String username, @Valid MessageCreationDTO messageCreationDTO) {
        messageService.sendMessageTo(username, messageMapper.toModelFromCreation(messageCreationDTO));
        return ResponseEntity.ok(ApiHelper.created("Message sent successfully"));
    }

    private List<MessageDTO> getMessageDTOS(List<Message> messageEntities) {
        List<MessageDTO> dtos = new LinkedList<>();
        messageEntities.forEach(m -> {
            dtos.add(messageMapper.toDto(m));
        });
        return dtos;
    }
}
