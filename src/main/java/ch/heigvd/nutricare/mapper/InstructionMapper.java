package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.FoodInDTO;
import ch.heigvd.nutricare.dto.InstructionCreationDTO;
import ch.heigvd.nutricare.dto.InstructionDTO;
import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Instruction;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class InstructionMapper extends Mapper<Instruction, InstructionDTO, InstructionCreationDTO> {
    @Override
    public Instruction toModel(InstructionDTO dtoObject) {
        return Instruction.builder()
                .idInstruction(dtoObject.getId())
                .instruction(dtoObject.getInstruction())
                .position(dtoObject.getPosition())
                .build();
    }

    @Override
    public InstructionDTO toDto(Instruction modelObject) {
        InstructionDTO i = new InstructionDTO();
        i.setId((int)modelObject.getIdInstruction());
        i.setInstruction(modelObject.getInstruction());
        i.setPosition(modelObject.getPosition());

        return i;
    }

    @Override
    public Instruction toModelFromCreation(InstructionCreationDTO creationObject) {
        return Instruction.builder()
                .instruction(creationObject.getInstruction())
                .position(creationObject.getPosition())
                .build();
    }
}
