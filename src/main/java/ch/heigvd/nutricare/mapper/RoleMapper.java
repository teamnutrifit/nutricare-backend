package ch.heigvd.nutricare.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ch.heigvd.nutricare.dto.RoleDTO;
import ch.heigvd.nutricare.model.user.role.ERole;
import ch.heigvd.nutricare.model.user.role.Role;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper extends Mapper<Role, RoleDTO, RoleDTO> {
    /**
     *
     * @param roles
     * @return
     * @author Alexandre Simik
     */
    public List<String> toStringRoles(Set<Role> roles) {
        List<String> stringRoles = new ArrayList<>();
        for (Role role : roles) {
            stringRoles.add(toStringRole(role));
        }
        return stringRoles;
    }

    /**
     *
     * @param role
     * @return
     * @author Alexandre Simik
     */
    private String toStringRole(Role role) {
        return role.getName().toString();
    }

    @Override
    public Role toModel(RoleDTO dtoObject) {
        return Role.builder()
                .roleId(dtoObject.getId())
                .name(ERole.getRole(dtoObject.getName().get()))
                .build();
    }

    @Override
    public RoleDTO toDto(Role modelObject) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId((int)modelObject.getRoleId());
        roleDTO.setName(JsonNullable.of(modelObject.getName().name()));
        return null;
    }

    @Override
    public Role toModelFromCreation(RoleDTO creationObject) {
        return null;
    }
}
