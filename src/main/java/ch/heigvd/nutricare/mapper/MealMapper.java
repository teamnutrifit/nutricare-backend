package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.FoodInDTO;
import ch.heigvd.nutricare.dto.MealCreationDTO;
import ch.heigvd.nutricare.dto.MealDTO;
import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.Meal;
import ch.heigvd.nutricare.model.user.User;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.LinkedList;
import java.util.List;

@Component
public class MealMapper extends Mapper<Meal, MealDTO, MealCreationDTO> {
    @Autowired
    private FoodInMapper foodInMapper;

    @Override
    public Meal toModel(MealDTO dtoObject) {
        List<FoodIn> foodList = new LinkedList<>();
        dtoObject.getFoods().forEach(w -> {
            foodList.add(foodInMapper.toModel(w));
        });
        return Meal.builder()
                .idMeal(dtoObject.getId())
                .name(dtoObject.getName())
                .dateTimeOfConsumption(dtoObject.getDateTimeOfConsumption().toLocalDateTime())
                .user(new User())
                .foodList(foodList)
                .build();
    }

    @Override
    public MealDTO toDto(Meal modelObject) {
        MealDTO mealDTO = new MealDTO();
        mealDTO.setId((int)modelObject.getIdMeal());
        mealDTO.setName(modelObject.getName());
        ZoneId zone = ZoneId.of("Europe/Paris");
        ZoneOffset zoneOffSet = zone.getRules().getOffset(modelObject.getDateTimeOfConsumption());

        mealDTO.setDateTimeOfConsumption(modelObject.getDateTimeOfConsumption().atOffset(zoneOffSet));

        List<FoodInDTO> foodList = new LinkedList<>();
        modelObject.getFoodList().forEach(w -> {
            foodList.add(foodInMapper.toDto(w));
        });
        mealDTO.setFoods(foodList);

        return mealDTO;
    }

    @Override
    public Meal toModelFromCreation(MealCreationDTO creationObject) {
        return Meal.builder()
                .name(creationObject.getName())
                .dateTimeOfConsumption(LocalDateTime.from(creationObject.getDateTimeOfConsumption()))
                .foodList(new LinkedList<>())
                .build();
    }
}
