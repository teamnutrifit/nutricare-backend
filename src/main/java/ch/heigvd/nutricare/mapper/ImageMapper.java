package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.ImageCreationDTO;
import ch.heigvd.nutricare.dto.ImageDTO;
import ch.heigvd.nutricare.model.image.Image;
import org.springframework.stereotype.Component;

@Component
public class ImageMapper extends Mapper<Image, ImageDTO, ImageCreationDTO> {
    @Override
    public Image toModel(ImageDTO dtoObject) {
        return Image.builder()
                .idImage(Long.valueOf(dtoObject.getId()))
                .image(dtoObject.getImage())
                .build();
    }

    @Override
    public ImageDTO toDto(Image modelObject) {
        ImageDTO imageDTO = new ImageDTO();

        imageDTO.setId((int)modelObject.getIdImage());
        imageDTO.setImage(modelObject.getImage());

        return imageDTO;
    }

    @Override
    public Image toModelFromCreation(ImageCreationDTO creationObject) {
        return Image.builder()
                .image(creationObject.getImage())
                .build();
    }
}
