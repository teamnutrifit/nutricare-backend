package ch.heigvd.nutricare.mapper;

public abstract class Mapper<M, D, C> {
    /**
     * Map the given DTO object to it's model equivalent
     * @param dtoObject the dto object to map
     * @return modelObject the model object equivalent to the given dto object
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public abstract M toModel(D dtoObject);

    /**
     * Map the given model object to it's DTO equivalent
     * @param modelObject the model object to map
     * @return dtoObject the dto object equivalent to the given model object
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public abstract D toDto(M modelObject);

    /**
     * Map the given DTO creation object to it's model equivalent
     * @param creationObject the DTO creation object to map
     * @return modelObject the model object equivalent to the given dto object
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public abstract M toModelFromCreation(C creationObject);
}
