package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.FoodInCreationDTO;
import ch.heigvd.nutricare.dto.FoodInDTO;
import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.utils.EUnit;
import ch.heigvd.nutricare.service.FoodService;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class FoodInMapper extends Mapper<FoodIn, FoodInDTO, FoodInCreationDTO> {
    @Autowired
    private FoodMapper foodMapper;

    @Autowired
    private FoodService foodService;

    @Override
    public FoodIn toModel(FoodInDTO dtoObject) {
        return FoodIn.builder()
                .idFoodIn(dtoObject.getId())
                .unit(EUnit.getUnit(dtoObject.getUnit()))
                .quantity(dtoObject.getQuantity().doubleValue())
                .food(foodMapper.toModel(dtoObject.getFood()))
                .build();
    }

    @Override
    public FoodInDTO toDto(FoodIn modelObject) {
        FoodInDTO f = new FoodInDTO();
        f.setId((int)modelObject.getIdFoodIn());
        f.setUnit(modelObject.getUnit().toString());
        f.setQuantity(BigDecimal.valueOf(modelObject.getQuantity()));
        f.setFood(foodMapper.toDto(modelObject.getFood()));

        return f;
    }

    @Override
    public FoodIn toModelFromCreation(FoodInCreationDTO creationObject) {
        return FoodIn.builder()
                .unit(EUnit.getUnit(creationObject.getUnit()))
                .quantity(creationObject.getQuantity().doubleValue())
                .food(foodService.getFoodByName(creationObject.getFood()))
                .build();
    }
}
