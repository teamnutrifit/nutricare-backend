package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.MessageCreationDTO;
import ch.heigvd.nutricare.dto.MessageDTO;
import ch.heigvd.nutricare.model.message.Message;

import ch.heigvd.nutricare.model.user.User;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageMapper extends Mapper<Message, MessageDTO, MessageCreationDTO> {
    @Autowired
    private UserMapper userMapper;

    @Override
    public Message toModel(MessageDTO dto) {
        return Message.builder()
                .idMessage(dto.getId())
                .title(dto.getTitle())
                .text(dto.getText())
                .hasBeenRead(dto.getHasBeenRead())
                .sender(userMapper.toModelFromSimple(dto.getSender()))
                .receptor(userMapper.toModelFromSimple(dto.getReceptor()))
                .build();
    }

    @Override
    public MessageDTO toDto(Message modelObject) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setId((int)modelObject.getIdMessage());
        messageDTO.setTitle(modelObject.getTitle());
        messageDTO.setText(modelObject.getText());
        messageDTO.setSender(userMapper.toSimple(modelObject.getSender()));
        messageDTO.setReceptor(userMapper.toSimple(modelObject.getReceptor()));
        messageDTO.setHasBeenRead(modelObject.isHasBeenRead());
        return messageDTO;
    }

    @Override
    public Message toModelFromCreation(MessageCreationDTO creationObject) {
        return Message.builder()
                .title(creationObject.getTitle())
                .text(creationObject.getText())
                .hasBeenRead(false)
                .receptor(new User())
                .sender(new User())
                .build();
    }

}
