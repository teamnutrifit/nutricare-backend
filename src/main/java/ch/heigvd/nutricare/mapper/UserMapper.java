package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.*;
import ch.heigvd.nutricare.model.user.User;
import ch.heigvd.nutricare.model.user.role.Role;
import ch.heigvd.nutricare.model.user.sex.ESex;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class UserMapper extends Mapper<User, UserDTO, UserDTO> {
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private ImageMapper imageMapper;

    @Override
    public User toModel(UserDTO dtoObject) {
        List<Role> roleList = new LinkedList<>();
        dtoObject.getRoles().forEach(roleDTO -> {
            roleList.add(roleMapper.toModel(roleDTO));
        });

        return User.builder()
                .idUser(dtoObject.getIdUser())
                .username(dtoObject.getUsername())
                .firstname(dtoObject.getFirstname())
                .lastname(dtoObject.getLastname())
                .birthdate(dtoObject.getBirthdate())
                .email(dtoObject.getEmail())
                .password(dtoObject.getPassword())
                .sex(ESex.getSex(dtoObject.getSex()))
                .roles(roleList)
                .build();
    }

    @Override
    public UserDTO toDto(User modelObject) {
        UserDTO userDTO = new UserDTO();
        userDTO.setIdUser((int) modelObject.getIdUser());
        userDTO.setUsername(modelObject.getUsername());
        userDTO.setFirstname(modelObject.getFirstname());
        userDTO.setLastname(modelObject.getLastname());
        userDTO.setBirthdate(modelObject.getBirthdate());
        userDTO.setEmail(modelObject.getEmail());
        userDTO.setPassword(modelObject.getPassword());
        userDTO.setSex(modelObject.getSex().name());
        userDTO.setImage(imageMapper.toDto(modelObject.getProfilePic()));

        List<RoleDTO> roleList = new LinkedList<>();
        modelObject.getRoles().forEach(role -> {
            roleList.add(roleMapper.toDto(role));
        });
        userDTO.setRoles(roleList);

        return userDTO;
    }

    @Override
    public User toModelFromCreation(UserDTO creationObject) {
        return null;
    }

    /**
     * Map a dto register object to it's user model equivalent
     * @param registerDTO the dto register object to map
     * @return userModel the user model created from the given dto register object
     * @author Clarisse Fleurimont <clarisse.fleu@gmail.com>
     */
    public User toModel(RegisterDTO registerDTO) {
        return User.builder()
                .username(registerDTO.getUsername())
                .firstname(registerDTO.getFirstname())
                .lastname(registerDTO.getLastname())
                .birthdate(registerDTO.getBirthdate())
                .email(registerDTO.getEmail())
                .password(registerDTO.getPassword())
                .sex(ESex.getSex(registerDTO.getSex()))
                .build();
    }

    /**
     * Map
     * @param user
     * @param jwt
     * @return
     * @author Alexandre Simik
     */
    public LoginSuccessDTO toLoginSuccessDTO(User user, String jwt) {
        LoginSuccessDTO loginSuccessDTO = new LoginSuccessDTO();
        loginSuccessDTO.setJwt(jwt);
        loginSuccessDTO.setUsername(user.getUsername());
        List<String> roles = new LinkedList<>();
        user.getRoles().forEach(role -> {
            roles.add(role.getName().toString());
        });

        loginSuccessDTO.setRoles(roles);
        return loginSuccessDTO;
    }

    public UserSimpleDTO toSimple(User user) {
        UserSimpleDTO userDTO = new UserSimpleDTO();
        userDTO.setUsername(user.getUsername());
        userDTO.setFirstname(user.getFirstname());
        userDTO.setLastname(user.getLastname());
        userDTO.setBirthdate(user.getBirthdate());
        userDTO.setEmail(user.getEmail());
        userDTO.setSex(user.getSex().name());
        return userDTO;
    }

    public User toModelFromModification(UserModificationDTO userModificationDTO) {
        return User.builder()
                .idUser(userModificationDTO.getIdUser())
                .username(userModificationDTO.getUsername())
                .firstname(userModificationDTO.getFirstname())
                .lastname(userModificationDTO.getLastname())
                .birthdate(userModificationDTO.getBirthdate())
                .email(userModificationDTO.getEmail())
                .password(userModificationDTO.getPassword())
                .sex(ESex.getSex(userModificationDTO.getSex()))
                .build();
    }

    public User toModelFromSimple(UserSimpleDTO userDTO) {
        return User.builder()
                .username(userDTO.getUsername())
                .firstname(userDTO.getFirstname())
                .lastname(userDTO.getLastname())
                .birthdate(userDTO.getBirthdate())
                .email(userDTO.getEmail())
                .sex(ESex.getSex(userDTO.getSex()))
                .password("")
                .build();
    }
}
