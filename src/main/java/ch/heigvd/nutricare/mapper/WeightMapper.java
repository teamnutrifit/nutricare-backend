package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.WeightCreationDTO;
import ch.heigvd.nutricare.dto.WeightDTO;
import ch.heigvd.nutricare.model.health.Weight;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class WeightMapper extends Mapper<Weight, WeightDTO, WeightCreationDTO> {

    @Override
    public Weight toModel(WeightDTO dtoObject) {
        return Weight.builder()
                .idWeight(dtoObject.getId())
                .weightDate(dtoObject.getWeightDate())
                .weight(dtoObject.getWeight().doubleValue())
                .build();
    }

    @Override
    public WeightDTO toDto(Weight modelObject) {
        WeightDTO weightDTO = new WeightDTO();
        weightDTO.setId((int)modelObject.getIdWeight());
        weightDTO.setWeight(BigDecimal.valueOf(modelObject.getWeight()));
        weightDTO.setWeightDate(modelObject.getWeightDate());
        return weightDTO;
    }

    @Override
    public Weight toModelFromCreation(WeightCreationDTO creationObject) {
        return Weight.builder()
                .weightDate(creationObject.getWeightDate())
                .weight(creationObject.getWeight().doubleValue())
                .build();
    }
}
