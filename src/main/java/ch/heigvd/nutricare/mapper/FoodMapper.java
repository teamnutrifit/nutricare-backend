package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.FoodCreationDTO;
import ch.heigvd.nutricare.dto.FoodDTO;
import ch.heigvd.nutricare.dto.FoodModificationDTO;
import ch.heigvd.nutricare.model.consommation.nutrition.Food;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class FoodMapper extends Mapper<Food, FoodDTO, FoodCreationDTO> {
    @Override
    public Food toModel(FoodDTO dtoObject) {
        return Food.builder()
                .idFood((long)dtoObject.getId())
                .name(dtoObject.getName())
                .description(dtoObject.getDescription())
                .carbohydrate(dtoObject.getCarbohydrate().doubleValue())
                .sugar(dtoObject.getSugar().doubleValue())
                .fat(dtoObject.getFat().doubleValue())
                .saturated(dtoObject.getSaturated().doubleValue())
                .protein(dtoObject.getProtein().doubleValue())
                .build();
    }

    @Override
    public FoodDTO toDto(Food modelObject) {
        FoodDTO foodDTO = new FoodDTO();
        foodDTO.setId((int)modelObject.getIdFood());
        foodDTO.setName(modelObject.getName());
        foodDTO.setDescription(modelObject.getDescription());
        foodDTO.setCarbohydrate(BigDecimal.valueOf(modelObject.getCarbohydrate()));
        foodDTO.setSugar(BigDecimal.valueOf(modelObject.getSugar()));
        foodDTO.setFat(BigDecimal.valueOf(modelObject.getFat()));
        foodDTO.setSaturated(BigDecimal.valueOf(modelObject.getSaturated()));
        foodDTO.setProtein(BigDecimal.valueOf(modelObject.getProtein()));

        // Kcal = 4Carb + 9Fat + 4*Prot
        foodDTO.setKcal(BigDecimal.valueOf(
                4 * modelObject.getCarbohydrate() +
                9 * modelObject.getFat() +
                4 * modelObject.getProtein()));

        return foodDTO;
    }

    @Override
    public Food toModelFromCreation(FoodCreationDTO creationObject) {
        return Food.builder()
                .name(creationObject.getName())
                .description(creationObject.getDescription())
                .carbohydrate(creationObject.getCarbohydrate().doubleValue())
                .sugar(creationObject.getSugar().doubleValue())
                .fat(creationObject.getFat().doubleValue())
                .saturated(creationObject.getSaturated().doubleValue())
                .protein(creationObject.getProtein().doubleValue())
                .build();
    }

    public Food toModelFromModification(FoodModificationDTO foodModificationDTO) {
        return Food.builder()
                .idFood(foodModificationDTO.getId())
                .name(foodModificationDTO.getName())
                .description(foodModificationDTO.getDescription())
                .carbohydrate(foodModificationDTO.getCarbohydrate().doubleValue())
                .sugar(foodModificationDTO.getSugar().doubleValue())
                .fat(foodModificationDTO.getFat().doubleValue())
                .saturated(foodModificationDTO.getSaturated().doubleValue())
                .protein(foodModificationDTO.getProtein().doubleValue())
                .build();
    }
}
