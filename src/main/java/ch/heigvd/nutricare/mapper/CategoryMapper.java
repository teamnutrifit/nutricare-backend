package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.*;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Category;
import ch.heigvd.nutricare.model.user.User;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Component
public class CategoryMapper extends Mapper<Category, CategoryDTO, CategoryCreationDTO> {
    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public Category toModel(CategoryDTO dtoObject) {
        List<User> users = new LinkedList<>();
        dtoObject.getUsers().forEach(userDTO -> {
            users.add(userMapper.toModelFromSimple(userDTO));
        });

        return Category.builder()
                .idCategory(dtoObject.getId())
                .name(dtoObject.getName())
                .users(users)
                .build();
    }

    @Override
    public CategoryDTO toDto(Category modelObject) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId((int)modelObject.getIdCategory());
        categoryDTO.setName(modelObject.getName());
        List<UserSimpleDTO> users = new LinkedList<>();
        modelObject.getUsers().forEach(user -> {
            users.add(userMapper.toSimple(user));
        });

        categoryDTO.setUsers(users);
        return categoryDTO;
    }

    @Override
    public Category toModelFromCreation(CategoryCreationDTO creationObject) {
        return Category.builder()
                .name(creationObject.getName())
                .users(new LinkedList<>())
                .build();
    }

    public Category toModelFromModification(CategoryModificationDTO categoryDTO) {
        return Category.builder()
                .idCategory(categoryDTO.getId())
                .name(categoryDTO.getName())
                .build();
    }
}
