package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.*;
import ch.heigvd.nutricare.model.consommation.nutrition.FoodIn;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Instruction;
import ch.heigvd.nutricare.model.consommation.nutrition.recipe.Recipe;
import ch.heigvd.nutricare.model.user.User;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class RecipeMapper extends Mapper<Recipe, RecipeDTO, RecipeCreationDTO> {
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private InstructionMapper instructionMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private FoodInMapper foodInMapper;

    @Override
    public Recipe toModel(RecipeDTO dtoObject) {
        List<Instruction> instructions = new LinkedList<>();
        dtoObject.getInstructions().forEach(instruction -> {
            instructions.add(instructionMapper.toModel(instruction));
        });
        List<FoodIn> foodList = new LinkedList<>();
        dtoObject.getFoods().forEach(w -> {
            foodList.add(foodInMapper.toModel(w));
        });
        List<User> subs = new LinkedList<>();
        dtoObject.getSubscribers().forEach(s -> {
            subs.add(userMapper.toModelFromSimple(s));
        });
        return Recipe.builder()
                .idRecipe(dtoObject.getId())
                .name(dtoObject.getName())
                .category(categoryMapper.toModel(dtoObject.getCategory()))
                .instructions(instructions)
                .subscribers(subs)
                .author(userMapper.toModelFromSimple(dtoObject.getAuthor()))
                .foodList(foodList)
                .build();
    }

    @Override
    public RecipeDTO toDto(Recipe modelObject) {
        RecipeDTO recipeDTO = new RecipeDTO();
        recipeDTO.setId((int)modelObject.getIdRecipe());
        recipeDTO.setName(modelObject.getName());
        recipeDTO.setAuthor(userMapper.toSimple(modelObject.getAuthor()));

        recipeDTO.setCategory(categoryMapper.toDto(modelObject.getCategory()));
        List<InstructionDTO> instructions = new LinkedList<>();
        modelObject.getInstructions().forEach(instruction -> {
            instructions.add(instructionMapper.toDto(instruction));
        });
        recipeDTO.setInstructions(instructions);

        List<FoodInDTO> foodList = new LinkedList<>();
        modelObject.getFoodList().forEach(w -> {
            foodList.add(foodInMapper.toDto(w));
        });
        recipeDTO.setFoods(foodList);

        List<UserSimpleDTO> subs = new LinkedList<>();
        modelObject.getSubscribers().forEach(w -> {
            subs.add(userMapper.toSimple(w));
        });
        recipeDTO.setSubscribers(subs);
        return recipeDTO;
    }

    @Override
    public Recipe toModelFromCreation(RecipeCreationDTO creationObject) {
        List<Instruction> instructions = new LinkedList<>();
        creationObject.getInstructions().forEach(instruction -> {
            instructions.add(instructionMapper.toModelFromCreation(instruction));
        });
        List<FoodIn> foodList = new LinkedList<>();
        creationObject.getFoods().forEach(w -> {
            foodList.add(foodInMapper.toModelFromCreation(w));
        });
        return Recipe.builder()
                .name(creationObject.getName())
                .category(categoryMapper.toModelFromCreation(creationObject.getCategory()))
                .instructions(instructions)
                .foodList(foodList)
                .build();
    }
}
