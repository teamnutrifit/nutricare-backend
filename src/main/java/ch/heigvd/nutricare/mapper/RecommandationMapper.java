package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.RecommandationDTO;
import ch.heigvd.nutricare.model.consommation.Recommandation;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class RecommandationMapper extends Mapper<Recommandation, RecommandationDTO, RecommandationDTO> {
    @Override
    public Recommandation toModel(RecommandationDTO dtoObject) {
        return Recommandation.builder()
                .idRecommandation(dtoObject.getId())
                .calories(dtoObject.getCalories().doubleValue())
                .imc(dtoObject.getImc().doubleValue())
                .water(dtoObject.getWater().doubleValue())
                .build();
    }

    @Override
    public RecommandationDTO toDto(Recommandation modelObject) {
        RecommandationDTO r = new RecommandationDTO();
        r.id((int)modelObject.getIdRecommandation());
        r.calories(BigDecimal.valueOf(modelObject.getCalories()));
        r.water(BigDecimal.valueOf(modelObject.getWater()));
        r.imc(BigDecimal.valueOf(modelObject.getImc()));
        return r;
    }

    @Override
    public Recommandation toModelFromCreation(RecommandationDTO creationObject) {
        return toModel(creationObject);
    }
}
