package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.WaterDTO;
import ch.heigvd.nutricare.dto.WaterModificationDTO;
import ch.heigvd.nutricare.model.consommation.Water;
import ch.heigvd.nutricare.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class WaterMapper extends Mapper<Water, WaterDTO, WaterDTO> {
    @Override
    public Water toModel(WaterDTO dtoObject) {
        return Water.builder()
                .dateOfConsumption(dtoObject.getDateOfComspution())
                .quantity(dtoObject.getQuantity().doubleValue())
                .idWater(dtoObject.getId())
                .user(new User())
                .build();
    }

    @Override
    public WaterDTO toDto(Water modelObject) {
        WaterDTO waterDTO = new WaterDTO();
        waterDTO.setDateOfComspution(modelObject.getDateOfConsumption());
        waterDTO.setQuantity(BigDecimal.valueOf(modelObject.getQuantity()));
        waterDTO.setId((int)modelObject.getIdWater());
        return waterDTO;
    }

    @Override
    public Water toModelFromCreation(WaterDTO creationObject) {
        return null;
    }

    public Water toModelFromModification(WaterModificationDTO waterDTO) {
        return Water.builder()
                .dateOfConsumption(waterDTO.getDateOfComspution())
                .quantity(waterDTO.getQuantity().doubleValue())
                .user(new User())
                .build();
    }
}
