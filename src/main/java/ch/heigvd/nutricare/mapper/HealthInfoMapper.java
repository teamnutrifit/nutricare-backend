package ch.heigvd.nutricare.mapper;

import ch.heigvd.nutricare.dto.HealthInfoCreationDTO;
import ch.heigvd.nutricare.dto.HealthInfoDTO;
import ch.heigvd.nutricare.dto.HealthInfoModificationDTO;
import ch.heigvd.nutricare.dto.WeightDTO;
import ch.heigvd.nutricare.model.health.HealthInfo;
import ch.heigvd.nutricare.model.health.Weight;
import ch.heigvd.nutricare.model.user.User;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Component
public class HealthInfoMapper extends Mapper<HealthInfo, HealthInfoDTO, HealthInfoCreationDTO> {
    @Autowired
    private WeightMapper weightMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public HealthInfo toModel(HealthInfoDTO dtoObject) {
        User user = userMapper.toModelFromSimple(dtoObject.getUser());
        List<Weight> weightEntities = new LinkedList<>();
        dtoObject.getWeights().forEach(w -> {
            weightEntities.add(weightMapper.toModel(w));
        });
        return HealthInfo.builder()
                .idHealthInfo(dtoObject.getId())
                .goal(dtoObject.getGoal())
                .activityLevel(dtoObject.getActivityLevel())
                .height(dtoObject.getHeight().doubleValue())
                .weightEntities(weightEntities)
                .user(user)
                .build();
    }

    @Override
    public HealthInfoDTO toDto(HealthInfo modelObject) {
        HealthInfoDTO healthInfoDTO = new HealthInfoDTO();
        healthInfoDTO.setId((int)modelObject.getIdHealthInfo());
        healthInfoDTO.setGoal(modelObject.getGoal());
        healthInfoDTO.setActivityLevel(modelObject.getActivityLevel());
        healthInfoDTO.setHeight(BigDecimal.valueOf(modelObject.getHeight()));
        healthInfoDTO.setUser(userMapper.toSimple(modelObject.getUser()));

        List<WeightDTO> weightDTOS = new ArrayList<>();
        for (Weight w :
                modelObject.getWeightEntities()) {
            weightDTOS.add(weightMapper.toDto(w));
        }
        healthInfoDTO.setWeights(weightDTOS);
        return healthInfoDTO;
    }

    @Override
    public HealthInfo toModelFromCreation(HealthInfoCreationDTO creationObject) {
        return HealthInfo.builder()
                .goal(creationObject.getGoal())
                .activityLevel(creationObject.getActivityLevel())
                .height(creationObject.getHeight().doubleValue())
                .user(new User())
                .build();
    }

    public HealthInfo toModelFromModification(HealthInfoModificationDTO healthInfoDTO) {
        return HealthInfo.builder()
                .idHealthInfo(healthInfoDTO.getId())
                .goal(healthInfoDTO.getGoal())
                .activityLevel(healthInfoDTO.getActivityLevel())
                .height(healthInfoDTO.getHeight().doubleValue())
                .user(new User())
                .build();
    }
}
