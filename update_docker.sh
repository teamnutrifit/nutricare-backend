#!/usr/bin/env bash

mvn clean install

cp ./target/*.jar ./docker/images/nutricare/artifact/app.jar

docker login --username=$DOCKER_USRNAME --password=$DOCKER_PSWD

echo "Update and push nutricare database"
cd docker/images/mysql/
docker build -t stellucidam/nutricare-ddb:debug .
docker push stellucidam/nutricare-ddb:debug

echo "Update and push nutricare app"
cd ../nutricare/
docker build -t stellucidam/nutricare-app:debug .
docker push stellucidam/nutricare-app:debug
