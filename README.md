![pipeline status](https://gitlab.com/teamnutrifit/nutricare-backend/badges/master/pipeline.svg)

# Nutricare Backend

## API
This repository concerns the backend of the Nutricare application. The information about the API can be found int the [wikis](https://gitlab.com/teamnutrifit/nutricare-backend/-/wikis/home) of the repository.

## Sources & Tutorials
[spring boot, vueJs, crud example](https://bezkoder.com/spring-boot-vue-js-crud-example/) \
[spring boot JPA crud rest api](https://bezkoder.com/spring-boot-jpa-crud-rest-api/) \
[JWT Authentication](https://bezkoder.com/spring-boot-vue-js-authentication-jwt-spring-security/) \
[JPA repositories](https://docs.spring.io/spring-data/jpa/docs/1.5.0.RELEASE/reference/html/jpa.repositories.html)


## Aide pour mise en place CI/CD
[CD for spring boot app (Kubernetes)](https://about.gitlab.com/blog/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/)
